# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer
Key stuff: Boxplot for each attribute for each class


Make a boxplot for each attribute for each class as shown in Figure 3.
Hints:
• Use the functions subplot() and boxplot().
• The variable y ∈ {0, . . . , C − 1} contains the class labels. To extract the
data objects belonging to, say, class c, you can use y to index into X like
this: X[(y==c).A.ravel(), :].
• It is easier to compare the boxplots if they are all on the same axis. To do
this, you can use the function ylim().


>>Show on the graph that all the Iris-setosa in this data set have a petal length
between 1 and 2 cm.

Looking at the graph, clearly we appreciate this.


"""

import xlrd
import numpy as np
from pylab import *

# EXTRACTION
wb     = xlrd.open_workbook('../Data/iris.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i))

# 50 samples from each of three species of Iris flowers  =  150 rows and 5 columns (4 parameters+1 description)
X = np.matrix(l).reshape(149,5)


# PLOTTING
classes = ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']

# Extract values for each Iris' class
class0 = np.array( X[(X[:,4]==classes[0]).ravel(),0:4],dtype=float) # Iris-setosa
class1 = np.array( X[(X[:,4]==classes[1]).ravel(),0:4],dtype=float) # Iris-virginica
class2 = np.array( X[(X[:,4]==classes[2]).ravel(),0:4],dtype=float) # Iris-versicolor

clases = [class0, class1, class2]

for i in xrange(len(clases)):
    subplot(1,3,i+1)    
    x0 = np.array(clases[i][:,0],dtype=float) # 'Sepal Length'
    x1 = np.array(clases[i][:,1],dtype=float) # 'Sepal Width'
    x2 = np.array(clases[i][:,2],dtype=float) # 'Petal Length'
    x3 = np.array(clases[i][:,3],dtype=float) # 'Petal Width'
    # building the data matrix
    x = [x0, x1, x2, x3]
    boxplot(x,widths=0.8)
    xlabels = ('Sepal Length','Sepal Width','Petal Length','Petal Width')
    xticks(range(0,4),xlabels,rotation=25)
    title('Class: {0}'.format(classes[i]))
    ylim(0,8)   
    
show()
