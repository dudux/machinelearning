# -*- coding: utf-8 -*-
"""

@author   : Eduardo Novella & Carlo Meijer
Key stuff : 3-dimensional scatter plot

Make a 3-dimensional scatter plot of three attributes as shown in Figure 5.

Hints:
• Read more about plotting in 3 dimensions: matplotlib.sourceforge.net/mpl_toolkits/mplot3d/tutorial.html.
• To plot in 3 dimensions, you need to import pylab as earlier, and additionally
Axes3D from mpl_toolkist.mplot3.


>>Try rotating the data. Can you find an angle where the three types of flower are
separated in the plot?

Yes, if we observe from above. Therefore, we are deleting 1-D

"""


import xlrd
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


# EXTRACTION
wb     = xlrd.open_workbook('../Data/iris.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i))

# 50 samples from each of three species of Iris flowers  =  150 rows and 5 columns (4 parameters+1 description)
X = np.matrix(l).reshape(149,5)


# PLOTTING
labels = ['Iris-setosa', 'Iris-virginica', 'Iris-versicolor']

# Extract values for each Iris' class
class0 = np.array( X[(X[:,4]==labels[0]).ravel(),0:4],dtype=float) # Iris-setosa
class1 = np.array( X[(X[:,4]==labels[1]).ravel(),0:4],dtype=float) # Iris-virginica
class2 = np.array( X[(X[:,4]==labels[2]).ravel(),0:4],dtype=float) # Iris-versicolor

classes = [class0, class1, class2]
colors  = ['b','r','g']

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

ax.set_xlabel('X Sepal Width')
ax.set_ylabel('Y Sepal Length')
ax.set_zlabel('Z Petal Length')

for i in xrange(len(classes)):
    x = np.array(classes[i][:,1],dtype=float) # 'Sepal Width'
    y = np.array(classes[i][:,2],dtype=float) # 'Petal Length'
    z = np.array(classes[i][:,3],dtype=float) # 'Petal Width'

    ax.scatter(x,y,z,color=colors[i])

plt.show()