# -*- coding: utf-8 -*-
"""

@author   : Eduardo Novella & Carlo Meijer

4.3.1 Load zipdata.mat by typing loadmat('Data/zipdata.mat'). There are two
data sets containing handwritten digits testdata and traindata. Here, we will
only use traindata. The first column in the matrix traindata contains the
digit (class) and the last 256 columns contain the pixel values.
Create the data matrix X and the class index vector y from the data. Remove
the digits 2-9 from the data, so only digits 0 and 1 are analyzed.

Next, compute the principal component analysis (PCA) of the data matrix. Now,
using the PCA, create a new data matrix X overwriting the old data matrix. The
new data matrix should have 4 attributes corresponding to PC1-PC4. Create
also the appropriate Python variables classNames, attributeNames, N, M, and
C.

Visualize this new data using the plots you created in Exercises 4.1.2-4.1.7.

Hints:
- Take a look back at Exercise 2.3.1 where you worked with the digits data
set before.
- Take a look back at Exercise 2.3.2 where you computed the principal com-
ponent analysis (PCA).

"""


import scipy.io
import scipy.linalg as linalg
from pylab import *
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

# extract data
traindata = scipy.io.loadmat('../Data/zipdata.mat')['traindata']
nums = traindata[:,0]
msk = (nums <= 1)
X = traindata[msk,1:]

Xc = X - np.ones((X.shape[0],1))*X.mean(0)

U,S,V = linalg.svd(Xc,full_matrices=False)
U = mat(U)
V = mat(V).T

# Compute variance explained by principal components
rho = (S*S) / (S*S).sum() 

# Project data onto principal component space
Z = Xc * V

# Plot variance explained
figure()
plot(rho,'o-')
title('Variance explained by principal components');
xlabel('Principal component');
ylabel('Variance explained value');

# Visualize the pricipal components
figure()
for k in range(4):
    N1 = ceil(sqrt(4)); N2 = ceil(4/N1)
    subplot(N2, N1, k+1)
    I = reshape(V[:,k], (16,16))
    imshow(I, cmap=cm.hot)
    title('PC{0}'.format(k+1));

show()