# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer
Key stuff: A matrix of scatter plots of each combination of two attributes.


4.2.3 Does any of the 12 attributes appear to correlate with each other? What plots
are well suited to investigate this?

For finding correlations, again the data matrix is likely the best method for finding patterns of correlation.
Since we are unable to use this functionality, we decided to draw 2d scatter plots between
all combinations of variables. In the case two variables are correlated, they yield a diagonal line shape.

We found that density and alcohol are correlated.
Residual sugar and density may also be correlated, but there is a clear distinction between two groups,
presumable indicating a red or a white wine.

"""

import scipy.io
import numpy as np
from pylab import *

mat = scipy.io.loadmat('../Data/wine.mat')
X = mat['X']
msk = (X[:,10]<=100)
X=X[msk,:]
msk = (X[:,7]<=2)
X=X[msk,:]
msk = (X[:,1]<=1000)
X=X[msk,:]

N = 12
for j in xrange(N):
    for k in xrange(N):
        subplot(N,N,j*N+k+1)
        x = np.array(X[:,k],dtype=float)
        y = np.array(X[:,j],dtype=float)

        plot(x, y, '.', markersize=.1)

show()



