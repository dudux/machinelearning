# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer

Load the data into Python using the scipy.io.loadmat() function. This data
set contains many observations that can be considered outliers. Use the visual-
ization tools you have worked with in the previous exercise to identify the outliers
in the data set. We have found 193 outliers in the data. Do you get to the same
result? When you have identified the outliers, remove them from the data set.

Hints:
- You can use your solutions to the previous exercise as a starting point for
making your visualizations.
- To find, for example, all data objects for which the alcohol percentage (at-
tribute number 11) is greater than 100%, you can mask them simply as
mask=(X[:,10]<=100).A.ravel().
- You can use the mask to eliminate the outlier observations (rows of data
matrix). For instance you can write X=X[mask,:] where mask indicates the
data objects that should be maintained. Remember also to remove them
from the class index vector, y=y[mask,1] and to recompute N.

"""

import scipy.io
import numpy as np
from pylab import *

mat = scipy.io.loadmat('../Data/wine.mat')
X = mat['X']
attribs = mat['attributeNames']


# 1) save original N
N = Norig = len(X)
print 'Norig : {0}'.format(N)

# 2) remove alc > 100%, which is obviously impossible
msk = (X[:,10]<=100)
X=X[msk,:]
N = len(X)
print 'N after removing alc > 100% : {0}'.format(N)

# 3) wine is water-based, density in g/cm^3 should be around 1
#    hence densities > 2 are extremely unlikely
#    the histogram shows the vast majority being in a plausible range,
#    with a few exceptions which are typically outliers
msk = (X[:,7]<=2)
X=X[msk,:]
N = len(X)
print 'N after removing density > 2 : {0}'.format(N)

# 4) this is measured in g/dm^3. Hence, entries with values around 1000
#    consists of almost pure acid (depending on density).
#    Wine shoud have a much lower value of acidity
msk = (X[:,1]<=1000)
X=X[msk,:]
N = len(X)
print 'N after removing acidity > 1000 : {0}'.format(N)

print 'Outliers removed : {0}'.format(Norig - N) # = 193

for y in xrange(3):
    for x in xrange(4):
        subplot(3,4,y*4+x+1)

        val = np.array(X[:,y*4+x],dtype=float)
        hist(val,bins=20)
        title(attribs[y*4+x][0][0])
        plot()

# The data looks plausibly distributed with the outliers removed

show()
