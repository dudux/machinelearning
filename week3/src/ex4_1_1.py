# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer
Key stuff: Import excel files in python

The Iris data set is available in the Excel file Data/iris.xls. Load the data into
Python and generate all the variables described in the table on “Representation
of data in Python” (see Exercise 2).

"""

import xlrd
import numpy as np

wb     = xlrd.open_workbook('../Data/iris.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i))

# 50 samples from each of three species of Iris flowers  =  150 rows and 5 columns (4 parameters)
X = np.matrix(l).reshape(149,5)

print X