# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer
Key stuff: Boxplot of the four attributes in the Iris data.


Make a boxplot of the four attributes in the Iris data as shown in Figure 2.
Hints:
• Take a look at the function boxplot().
• Type help(boxplot) to see how you can adjust the boxplot.
This boxplot shows the same information as the histogram in the previous exer-
cise. 


>>>Discuss the advantages and disadvantages of the two types of plots.

Boxplots allow us to observe how data is distributed. Data is splitted into a box and 2 whiskers,
also marking outliers from the distribution. The distance between quartiles represents the distribution of the data. 
For example, short whiskers represent clustered coordinates and long whiskers represent spread-out coordinates.
Boxplots enable us to quickly see the the median, smallest/largest observation, lower/upper quartile and outliers.

Histograms have the advantage of showing in more detail how the data is distributed. 
For example, the fact that no petal lengths exists between 2 and 3cm cannot be seen fron the boxplot. However,
the median, smallest/largest observation, lower/upper quartile and outliers are not immediately evident.
"""

import xlrd
import numpy as np
from pylab import *

# EXTRACTION
wb     = xlrd.open_workbook('../Data/iris.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i))

# 50 samples from each of three species of Iris flowers  =  150 rows and 5 columns (4 parameters+1 description)
X = np.matrix(l).reshape(149,5)


# PLOTTING
figure(1)
x0 = np.array(X[:,0],dtype=float) 
x1 = np.array(X[:,1],dtype=float)
x2 = np.array(X[:,2],dtype=float)
x3 = np.array(X[:,3],dtype=float)

# building the data matrix
x = [x0, x1, x2, x3]

boxplot(x)
xlabels = ('Sepal Length','Sepal Width','Petal Length','Petal Width')
xticks(range(1,5),xlabels)
title('Fisher\'s Iris dataset -- boxplot')
    
show()