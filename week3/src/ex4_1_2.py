# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer
Key stuff: Import excel files in python & plot them by using histogram

Plot a histogram of each of the four attributes in the Iris data as shown in Figure1.
Figure 1: Histogram of each of the four attributes in the Iris data



>> Show on the graph that the petal length is either between 1 and 2 cm or between
3 and 7 cm, but that no flowers in the data set have a petal length between 2
and 3 cm. Do you think this could be useful to discriminate between the different
types of flowers?

We cannot tell this from the histogram alone, it may be the case that a type of flower
exists with both short and long petal lengths, but none in between. Hence,
it need not be a property that characterizes the type of flower.

"""

import xlrd
import numpy as np
import matplotlib.pyplot as plt

# EXTRACTION
wb     = xlrd.open_workbook('../Data/iris.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i))

# 50 samples from each of three species of Iris flowers  =  150 rows and 5 columns (4 parameters+1 description)
X = np.matrix(l).reshape(149,5)


# PLOTTING
xlab    = ['Sepal Length','Sepal Width','Petal Length','Petal Width']
xcolors = ['green','violet','gray','blue']

for i in xrange(4):
    plt.subplot(2,2,i+1)
    plt.xlabel(xlab[i])
    x1 = np.array(X[:,i],dtype=float)
    plt.hist(x1,color=xcolors[i])
    plt.plot()
    
plt.show()
