# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer
Key stuff: A matrix of scatter plots of each combination of two attributes.


Make a matrix of scatter plots of each combination of two attributes against
each other as shown in Figure 4.

Hints:
• To make a scatter plot, you can use the function plot(x,y,s) where x and
y specify the coordinates and s is a string that specifies the line style and
plot symbol, e.g., s=’.’ to make dots.
• To extract the data values for the m’th attribute in the c’th class, you can
write X[(y==c).A.ravel(),m].
• You can use the function hold() to plot multiple plots on top of each other.


>>Say you want to discriminate between the three types of flowers using only the
length and width of either sepal or petal. Show on the graph why it would be
better to use petal length and width rather than sepal length and width. 

The scatter plots of petal width/length show clear clusters for each of
the Iris flowers. When using sepal width/length, Iris-versicolor and Iris-virginica
cannot be reliably distinguished.

"""

import xlrd
import numpy as np
from pylab import *

def scatterplot():
    N = 4
    for j in xrange(N):
        for k in xrange(N):
            subplot(4,4,j*N+k+1)
            for i in xrange(len(classes)):
                x = np.array(classes[i][:,k],dtype=float)
                y = np.array(classes[i][:,j],dtype=float)

                plot(x, y, '.')
        
    show()

# EXTRACTION
wb     = xlrd.open_workbook('../Data/iris.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i))

# 50 samples from each of three species of Iris flowers  =  150 rows and 5 columns (4 parameters+1 description)
X = np.matrix(l).reshape(149,5)


# PLOTTING
labels = ['Iris-setosa', 'Iris-virginica', 'Iris-versicolor']

# Extract values for each Iris' class
class0 = np.array( X[(X[:,4]==labels[0]).ravel(),0:4],dtype=float) # Iris-setosa
class1 = np.array( X[(X[:,4]==labels[1]).ravel(),0:4],dtype=float) # Iris-virginica
class2 = np.array( X[(X[:,4]==labels[2]).ravel(),0:4],dtype=float) # Iris-versicolor

classes = [class0, class1, class2]

scatterplot()


