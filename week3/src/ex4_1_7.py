# -*- coding: utf-8 -*-
"""

@author   : Eduardo Novella & Carlo Meijer
Key stuff : Plot the data matrix as an image

Plot the data matrix as an image as shown in Figure 6. The data matrix should
be standardized to have zero mean and unit standard deviation.
Hints:
• You can use the function imagesc() to plot an image.
• By default, the image will be smoothed, which is not always desired when
you look at the data. Use parameter interpolation=’None’ to display
raw data.
• The function zscore() can be used to standardize the data matrix.

You are welcome to try out other plotting methods for the data. The matplotlib on-
line repository is a good source of inspiration: matplotlib.sourceforge.net/gallery.
html.

"""


import xlrd
import numpy as np
from pylab import *
import matplotlib.pyplot as plt
from scipy import stats



# EXTRACTION
wb     = xlrd.open_workbook('../Data/iris.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i))

# 50 samples from each of three species of Iris flowers  =  150 rows and 5 columns (4 parameters+1 description)
X = np.matrix(l).reshape(149,5)


# PLOTTING

# The z-scores, standardized by mean and standard deviation of input array a
sl = stats.zscore(np.array(X[:,0],dtype=float))
sw = stats.zscore(np.array(X[:,1],dtype=float))
pl = stats.zscore(np.array(X[:,2],dtype=float))
pw = stats.zscore(np.array(X[:,3],dtype=float))

a = [sl,sw,pl,pw]

title('Fisher\'s Iris data Matrix')
xlabel = ['Sepal Length','Sepal Width','Petal Length','Petal Width']
ylabel('Data objects')

for i in xrange(len(xlabel)):
    plt.imshow(a[i], interpolation='None')


plt.show()