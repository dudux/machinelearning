# -*- coding: utf-8 -*-
"""

@author   : Eduardo Novella & Carlo Meijer
Key stuff : 3-dimensional scatter plot

4.2.4 Can you identify any clear relationship between the various physicochemical mea-
surements of the wines and the quality of the wines as rated by human judges?

CAUTION: takes long to run!

We used the 2d scatter plots from the last exercise to indicate whether the judgment
appears to be correlated with any of the variables. This seems to be the case with
alcohol and density (which is correlated with alcohol).

In order to find any combinations of variables yielding a correlation with the judgment,
we also generate 3d scatterplots for each combination of variables on the x and y axis,
and the rating on the z axis. The same pattern is visible again: alcohol and density separately
correlate with the judgment. However, we do not see a combination of variables that together
seem to correlate with the judgment.

"""


import scipy.io
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

mat = scipy.io.loadmat('../Data/wine.mat')
X = mat['X']
msk = (X[:,10]<=100)
X=X[msk,:]
msk = (X[:,7]<=2)
X=X[msk,:]
msk = (X[:,1]<=1000)
X=X[msk,:]

fig = plt.figure()

N = 11
for j in xrange(N):
    for k in xrange(N):
        #subplot(N,N,j*N+k+1,projection='3d')
        ax = fig.add_subplot(N,N,j*N+k+1, projection='3d')
        x = np.array(X[:,k],dtype=float)
        y = np.array(X[:,j],dtype=float)
        z = np.array(X[:,11],dtype=float)

        ax.scatter(x,y,z)

#for i in xrange(len(classes)):
    #x = np.array(classes[i][:,1],dtype=float) # 'Sepal Width'
    #y = np.array(classes[i][:,2],dtype=float) # 'Petal Length'
    #z = np.array(classes[i][:,3],dtype=float) # 'Petal Width'

    #ax.scatter(x,y,z,color=colors[i])

plt.show()