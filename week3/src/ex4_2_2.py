# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer

4.2.2 Are there any of the measurements that seem to be well suited in order to
discriminate between red and white wines? What plots are particular useful in
order to investigate this?

We want to perform some kind of clustering.
Histograms should be very well-suited for this, clearly indicating two groups of data,
which are, presumably, red and white wines.
However, the histograms from the last assignment don't indicate a clear division.
Hence, with so many attributes, the data matrix image is likely the best
method for visualizing differences between groups.
When attributes are found with high variance between the two groups,
we can use a (3d-)scatter plot to view the differences.

Unfortunately, imagesc is not defined in python
and imshow does not work on the data for some reason

In the next exercise we will use 2d scatter plots for finding correlations
between the two of the 12 variables. They can also be used reasonably well
for distinguishing between red and white. In the case two distinct blobs appear,
this indicates that there exists two groups that can be distinguished by two variables.
We find that sulfur dioxide can reasonably distinguish two groups,
when plotted against either density, pH, sulphates or alcohol.
We suspect they indicate a red or a white wine.
We also find that two blobs appear when plotting residual sugar against density,
which may distinguish the same.

The same  is true for 3d scatter plots, with 3 variables. However,
it seems infeasible to draw 1728 plots.

"""

import scipy.io
import numpy as np
from pylab import *

#mat = scipy.io.loadmat('../Data/wine.mat')
#X = mat['X']
#msk = (X[:,10]<=100)
#X=X[msk,:]
#msk = (X[:,7]<=2)
#X=X[msk,:]
#msk = (X[:,1]<=1000)
#X=X[msk,:]

#fig = figure()
#ax = fig.add_subplot(1,1,1)
#ax.set_aspect(2)
#imshow(X,interpolation='None')
#colorbar()
#show()
