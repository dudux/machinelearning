# -*- coding: utf-8 -*-
"""
@author   : Eduardo Novella & Carlo Meijer
key stuff : Masking to remove outliers

Load the wine data set into Python. Remove outliers from the data set. We will
here define outliers as data objects where 
Volatile acidity > 20 g/dm3 , Density > 10 g/cm3 , or Alcohol > 200%. 
Remove attribute 12, the Quality score, from the data set, in order to use only the quantitative
 measurements for the prediction.


Hints:
-- You have worked with the wine data before in Exercise 4.2.1 You can use
your solution to that exercise as a starting point.

-- To select a subset of data objects from the data matrix, you can write
X=X[n,:], where n is a vector of indices to data objects to be selected, or
a binary mask. Remember also to remove them from the class index vector,
y=y[n,:] and to recompute N.
-- Similarly you can select a subset of the attributes from the data matrix
X=X[:,m] where m is either a vector of indices to the attributes, or a binary
mask. Remember to update the list of attribute names accordingly, and to
recompute the number of attributes M.

You should end up with a data matrix of size N × M = 6304 × 11.
"""

import scipy.io

# Load data
mat     = scipy.io.loadmat('../../week3/Data/wine.mat')
X       = mat['X']
y       = mat['y']
attribs = mat['attributeNames']

def cleanOutliers(X,y=None):
    '''
    Function to remove outliers
    '''
    # Remove from class index vector. Remove attribute 12, the Quality score, from the data set
    X = X[:,0:11]
    
    # save original N
    N = Norig = len(X)
    print '{0:40s}: {1}'.format('Norig',N)
    
    # Select a subset of data objects. 
    # Remove Outliers are Volatile acidity > 20 g/dm3 , Density > 10 g/cm3 , or Alcohol > 200%. 
    msk = (X[:,1]<=20)  # volatile acidity
    X   = X[msk,:]
    if(y != None):
        y = y[msk]
    N   = len(X)
    print '{0:40s}: {1}'.format('N after Volatile acidity > 20 g/dm3' ,N)
    
    msk = (X[:,7]<=10)  # density
    X   = X[msk,:]
    if(y != None):
        y = y[msk]
    N   = len(X)
    print '{0:40s}: {1}'.format('N after Density > 10 g/cm3',N)
    
    msk = (X[:,10]<=200) # alcohol
    X   = X[msk,:]
    if(y != None):
        y = y[msk]
    N   = len(X)
    print '{0:40s}: {1}'.format('N after Alcohol > 200%',N)
    
    # Recompute N
    print '{0:40s}: {1}'.format('Outliers removed',Norig-N)
    
    return (X,y)
    
def main():
    cleanOutliers(X)
    
if __name__ == "__main__":
    main()
