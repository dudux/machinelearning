# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer

Make a scatter plot of your data. Does it look like the figure below?

Yes

"""

import numpy as np
from pylab import *

x = range(100)
y = map(lambda x: -.5 + 0.01 * x + np.random.randn() * 0.1, x)

plot(x,y,'.')
show()
