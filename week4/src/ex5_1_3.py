# -*- coding: utf-8 -*-
"""
@author   : Eduardo Novella & Carlo Meijer
key stuff :  Fitting of a decision tree && prediction
    
Write code that shows that a dragon, which is cold-blooded, has scales, gives
birth, is semi-aquatic, is an aerial creature, has legs, and hibernates, will be
classiffied as a reptile.

"""
import numpy as np
from sklearn import tree
import ex5_1_1 as data

attributes = '''
###################
# Attribute Unit  #
###################
1.- 'Body temperature' 0:Cold blooded, 1:Warm blooded
2.- 'Skin cover'       0:None, 1:Hair, 2:Scales, 3:Feathers, 4:Fur, 5:Quills
3.- 'Gives birth'      0:No, 1:Yes
4.- 'Aquatic creature' 0:No, 1:Yes, 2:Semi
5.- 'Aerial creature'  0:No, 1:Yes
6.- 'Has legs'         0:No, 1:Yes
7.- 'Hibernates'       0:No, 1:Yes.
'''

# Import data
X  = np.asarray(data.X)
y  = np.ravel(data.y)

# Create tree and fitting
tr = tree.DecisionTreeClassifier()
tr.fit(X,y)

# dragon, which is cold-blooded, has scales, gives birth, is semi-aquatic, is an aerial creature, has legs, and hibernates==reptile
dragon = np.array([0,2,1,2,1,1,1])
tr.predict(dragon)

# Output
print attributes
print '\n\nA dragon with {0} should be a : {1}'.format(dragon,data.classNames[tr.predict(dragon)[0]])
