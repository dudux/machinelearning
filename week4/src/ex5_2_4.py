# -*- coding: utf-8 -*-
"""
Load the wine data into Python using your solution to Exercise 5.1.5. Fit a linera
model that predicts the Alcohol attribute based on the 10 remaining attributes
(again excluding Quality score). Make a scatter plot of the true versus the
predicted alcohol content. Show that the alcohol content can be predicted with
an accuracy of approximately +/-1 percentage point.

The bottom scatter plot shows that the difference between the actual value and the prediction
roughly does not exceed 1%. The mean absolute error is 0.38%

Show from the estimated parameters of the linear model that wines
with higher alcohol content have lower density

The coefficient for density found by the least squares fitting is -631.055184,
which is negative. Hence, in general, wines with higher alcohol content have lower density.
"""

import numpy as np
from sklearn import linear_model
from sklearn import metrics
import ex5_1_4 as data
from pylab import *

#  Import data
dat = data.cleanOutliers(data.X)[0]
y = dat[:,10]
X = dat[:,:10]

m = linear_model.LinearRegression(fit_intercept=True)
m.fit(X,y)

print 'w0 : {0}\nwx : {1}'.format(m.intercept_, m.coef_)

p = m.predict(X)

print 'mae : {0}'.format(metrics.mean_absolute_error(y, p))

subplot(2,1,1)
plot(y,p,'.')
subplot(2,1,2)
plot(y, np.subtract(y,p), '.')

show()
