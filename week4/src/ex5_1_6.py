# -*- coding: utf-8 -*-
"""
@author   : Eduardo Novella & Carlo Meijer
key stuff : Decision trees and prediction 

Show that a wine with the following attribute values would be classified as white.

          Attribute                  Value
          Fixed acidity (tartaric)   6.9 g/dm3
          Volatile acidity (acetic)  1.09 g/dm3
          Citric acid                0.06 g/dm3
          Residual sugar             2.1 g/dm3
          Chlorides                  0.0061 g/dm3
          Free sulfur dioxide        12 mg/dm3
          Total sulfur dioxide       31 mg/dm3
          Density                    0.99 g/cm3
          pH                         3.5
          Sulphates                  0.44 g/dm
          Alcohol                    12%

"""
import scipy.io
import numpy as np
from sklearn import tree

# Load data
mat        = scipy.io.loadmat('../../week3/Data/wine.mat')
X          = mat['X']
y          = mat['y']
attribs    = mat['attributeNames']
classNames = mat['classNames']

#''' Optional '''
#from pylab import *
## Plot according total sulfur dioxide
#plot(X[:,6], '.')
#xlabel(''); ylabel('Total sulfur dioxide')
#title('Scatter plot of sulfur dioxide')
#show()

# Parsing to np.array
X = np.array(X)
y = np.ravel(np.array(y))

# Create tree and fitting
tr = tree.DecisionTreeClassifier(criterion='gini')
tr.fit(X,y)

values  = [6.9, 1.09, 0.06, 2.1, 0.0061, 12, 31, 0.99, 3.5, 0.44, 12, 5.0] # added 12-column to 5.0
iswhite = np.array(values) 
tr.predict(iswhite)
print '\n\nThis wine with {0}\n\nshould be : {1}'.format(values,classNames[tr.predict(iswhite)[0]]) # TODO it's returning the classesarray with 2 values? It should be only one