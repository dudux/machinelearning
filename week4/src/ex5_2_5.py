# -*- coding: utf-8 -*-
"""
Try to include more variables in the regression by combining or transforming
existing variables

Hints:
 - For example you can include the square of the Fixed acidity in the model
   and the square of the Volatile as well as a multiplicative interaction between
   Fixed acidity and Volatile acidity. You can use the numpy.power() and
   numpy.multiply() functions on appropriate columns of the data matrix.
 - The function numpy.bmat() is useful to concatenate matrices and/or vec-
   tors, for instance: X=np.bmat('X, Xfa2, Xva2, Xfava').
 - Plots of attributes versus the model residuals can reveal if there is structure
   in the output that can be explained by transformations of the attributes.

We expected the residuals (bottom graph) to show a parabolic shape in case
of a squared dependency. However, no such shape is observable,
even when not taking Xfa2, Xva2 and Xfava into consideration.
Therefore, to the best of our knowledge, squaring, multiplying, etc.
other attributes can only be done in a brute force fashion.
Hence, we did not include any other variables.

How low can you get the mean squared error?
The mean squared error is 0.24.

"""

import numpy as np
from sklearn import linear_model
from sklearn import metrics
import ex5_1_4 as data
from pylab import *

#  Import data
dat = data.cleanOutliers(data.X)[0]
y = dat[:,10]
X = dat[:,:10]

Xfa2 = matrix([np.power(X[:,0], 2)]).transpose()
Xva2 = matrix([np.power(X[:,1], 2)]).transpose()
Xfava = matrix([np.multiply(X[:,0], X[:,1])]).transpose()
X = np.bmat('X, Xfa2, Xva2, Xfava')


m = linear_model.LinearRegression(fit_intercept=True)
m.fit(X,y)

print 'w0 : {0}\nwx : {1}'.format(m.intercept_, m.coef_)

p = m.predict(X)

print 'mse : {0}'.format(metrics.mean_squared_error(y, p))

subplot(3,1,1)
plot(y,p,'.')
subplot(3,1,2)
plot(y, np.subtract(y,p), '.')
subplot(3,1,3)
for t in range(10):
    plot(X[:,t], np.subtract(y,p), '.')
show()
