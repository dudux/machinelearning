# -*- coding: utf-8 -*-
"""
Load the wine data into Python using your solution to Exercise 5.1.4. Fit a
logistic regression model that predicts the type of wine (red or white) based on
the attributes 1-11 (again excluding Quality score). Show that the wine from
Exercise 5.1.6 would be classified as white with more that 98% probability.

The probability we found is 0.9987
"""

import numpy as np
from sklearn import linear_model
import ex5_1_4 as data

#  Import data
(X, y) = data.cleanOutliers(data.X, data.y)

m = linear_model.LogisticRegression(fit_intercept=True)
m.fit(X,y)

p = m.predict_proba([6.9, 1.09, 0.06, 2.1, 0.0061, 12, 31, 0.99, 3.5, 0.44, 12])
print 'probability of wine being white : {0}'.format(p[0,0])
