# exercise 5.2.1

'''
Inspect and run the script ex5_2_3.py which generates a data set according to
the linear regresion model including the squared term, fits the model, and makes
a plot as in the previous excercise. The script allows you to specify separately
how many terms to include (squared, cubed, etc.) for generating the data and
fitting the model. Illustrate what happens when fitting a model with more higher
order terms? How well is the model able to fit the data?

When fitting with a higher order of terms, the predictions no longer approach the
model any further. Instead, the model is more susceptible to the noise.

'''

from pylab import *
import numpy as np
import sklearn.linear_model as lm

# Parameters
Kd = 5  # no of terms for data generator
Km = 1  # no of terms for regression model
N = 50  # no of data objects to train a model
Xe =  np.mat(linspace(-2,2,1000)).T # X values to visualize true data and model
eps_mean, eps_std = 0, 0.5          # noise parameters

# Generate dataset (with noise)
X = np.mat(linspace(-2,2,N)).T
Xd = np.power(X, range(1,Kd+1))
eps = np.mat(eps_std*np.random.randn(N) + eps_mean).T
w = np.mat( -np.power(-.9, range(1,Kd+2)) ).T
y = w[0,0] + Xd * w[1:,:] + eps 

# True data generator (assuming no noise)
Xde = np.power(Xe, range(1,Kd+1))
y_true = w[0,0] + Xde * w[1:,:]


# Fit ordinary least squares regression model
Xm = np.power(X, range(1,Km+1))
model = lm.LinearRegression()
model = model.fit(Xm,y)

# Predict values
Xme = np.power(Xe, range(1,Km+1))
y_est = model.predict(Xme)

# Plot original data and the model output
f = figure()
f.hold(True)
plot(X,y,'.')
plot(Xe,y_true,'-')
plot(Xe,y_est,'-')
xlabel('X'); ylabel('y'); ylim(-2,8)
legend(['Training data', 'Data generator K={0}'.format(Kd), 'Regression fit (model) K={0}'.format(Km)])

show()

