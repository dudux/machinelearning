# -*- coding: utf-8 -*-
"""
Fit a decision tree to the wine data in order to estimate if the wine is red or white.
Use the Gini splitting criterion. Use min_samples_split=100 for the stopping
criterion. After fitting the tree, export its structure to GraphViz format.
Hints:
-- Use your solution to Exercise 5.1.2 as a starting point.


>>Explain what happens when you change the values of the parameter min_samples_split.
Observe the changes in the tree size after reducing the parameter.

"According to :
    min_samples_split : integer, optional (default=2)
    The minimum number of samples required to split an internal node."

This parameter is useful to take over control about number of samples at leaf nodes.
When we tweak this value to small values means that the three tends to be balanced  and overfit, 
whereas a higher value means will be difficult to predict.



  Atribute                     Unit
1 Fixed acidity (tartaric)     g/dm3
2 Volatile acidity (acetic)    g/dm3
3 Citric acid                  g/dm3
4 Residual sugar               g/dm3
5 Chlorides                    g/dm3
6 Free sulfur dioxide          mg/dm3
7 Total sulfur dioxide         mg/dm3
8 Density                      g/cm3
9 pH                           pH
10 Sulphates                   g/dm3
11 Alcohol                     % vol
12 Quality score               0-10

"""

import scipy.io
import numpy as np
from sklearn import tree

# Load data
mat             = scipy.io.loadmat('../../week3/Data/wine.mat')
X               = mat['X']
y               = mat['y']
attributesNames = mat['attributeNames']

# Parsing to np.array
X = np.array(X)
y = np.ravel(np.array(y))

# Create tree and fitting
tr = tree.DecisionTreeClassifier(criterion='gini', min_samples_split=100)
tr.fit(X,y)

# Graphic output 
# dot -Tpng wine.dot -o wine.png
with open("wine100.dot", 'w') as f:
    tree.export_graphviz(tr, feature_names=attributesNames,out_file=f)