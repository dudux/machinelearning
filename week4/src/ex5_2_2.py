# -*- coding: utf-8 -*-
"""

@author: Eduardo Novella & Carlo Meijer

"""

import numpy as np
import scipy
from sklearn import linear_model
from pylab import *

f = lambda x: -.5 + 0.01 * x + np.random.randn() * 0.1

x = range(100)
y = map(f, x)

m = linear_model.LinearRegression(fit_intercept=True)
X = np.matrix([x]).transpose()

m.fit(X,y)

w0 = m.intercept_
w1 = m.coef_[0]

print 'w0 : {0}\nw1 : {1}'.format(w0,w1)

p = m.predict(X)

plot(x, y, '.')
plot(x, p, '.')

show()