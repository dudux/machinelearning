# -*- coding: utf-8 -*-
"""
@author   : Eduardo Novella & Carlo Meijer
key stuff :  Fitting of a decision tree
    
Fit a decision tree to the vertebrates data. Use the Gini (gdi) splitting criterion,
and stop splitting only when nodes are pure. After fitting the tree, export its structure to GraphViz format.

sklearn.tree.DecisionTreeClassifier

>> Does your result look like the tree below?

"""

import numpy as np
from sklearn import tree
import ex5_1_1 as data

# Import data
X  = np.asarray(data.X)
y  = np.ravel(data.y)

# Create tree and fitting
tr = tree.DecisionTreeClassifier()
tr.fit(X,y)

# Graphic output 
# dot -Tpng tree.dot -o tree.png
with open("tree.dot", 'w') as f:
    tree.export_graphviz(tr, feature_names=data.attributeNames,out_file=f)