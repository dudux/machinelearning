# exercise 3.3.5

'''
Inspect and run the script ex3_3_5.py which generates 2-dimensional normally
distributed random samples and plots a scatter plot as well as a 2-dimensional
histogram. In the script, the covariance matrix is constructed from the standard
deviations and correlation as described above. 

a) Show that when the correlation between x1 and x2 is zero, the scatter plot and 2-d histogram have the shape of
an axis-aligned ellipse. Can you explain why?
> Whether the correlation is zero, then covariance matrix is also zero. 
That means that x1 and x2 are independent random variables or in other words that 
there is no linear relation between x1 and x2. Moreover, the correlation coefficient is unitless value
and it is between [-1,1] and it is useful to measure covariance avoiding units. As zero is in the middle between -1,1 we cannot appreciate any correlation and for that reason
the shape is a kind of ellipse or so. The linear relation is zero, there is not relationship between those variables.



b) Show that when the correlation between x1 and x2 is one, the values of x1 and x2
fall on a straight line. Can you explain why? 
> The correlation coefficient shows  how close two variables lie along a line. If this is 1, that means that
x1 and x2 means that the two variables (x1,x2) move into the same direction. If it would be -1 , the 2 variables move into 
opposite direction.



c) Try varying the number of samples,the mean, the standard deviations, the correlation and the number of histogram
bins and see what happens.

> modifying :
    number of samples = we see more density of points
    mean              = Graphs look similar
    std.devitations   = dispersion takes differences

'''

from pylab import *
import numpy as np

# Number of samples
N = 1000

# Standard deviation of x1
s1 = 2

# Standard deviation of x2
s2 = 3

# Correlation between x1 and x2
corr = 0.5

# Covariance matrix
S = np.matrix([[s1*s1, corr*s1*s2], [corr*s1*s2, s2*s2]])

# Mean
mu = np.array([13, 17])

# Number of bins in histogram
nbins = 20

# Generate samples from multivariate normal distribution
X = np.random.multivariate_normal(mu, S, N)


# Plot scatter plot of data
figure(figsize=(12,8))
title('2-D Normal distribution')

subplot(1,2,1)
plot(X[:,0], X[:,1], 'x')
xlabel('x1'); ylabel('x2')
title('Scatter plot of data')

subplot(1,2,2)
x = histogram2d(X[:,0], X[:,1], nbins)
imshow(x[0], cmap=cm.gray_r, interpolation='None', origin='lower')
colorbar()
xlabel('x1'); ylabel('x2'); xticks([]); yticks([]);
title('2D histogram')

show()