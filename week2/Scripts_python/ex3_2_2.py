# -*- coding: utf-8 -*-
"""
Created on Thu Feb 13 22:54:44 2014


We will investigate how scaling and translation impact the following three simi-
larity measures: Cosine, ExtendedJaccard, and Correlation. Let alpha and beta be two
constants. 

Either show that an equality does not hold be finding a counterexample, or give
a proof that they are equal. 

Which of the following statements are correct?

Being alpha and beta equal 2. 
e.g sim1  means cosine(x,y) and sim2 means cosine(2*x,y) or cosine(2+x,y):

Proofs are given in ex3_2_2.pdf

COSINE
################    
sim1 = (X*Y.T)/(np.sqrt(sum(np.power(X.T,2))).T * np.sqrt(sum(np.power(Y.T,2))))
sim2 = (alpha*X*Y.T)/(np.sqrt(sum(np.power(alpha*X.T,2))).T * np.sqrt(sum(np.power(Y.T,2))))
1.- Cosine(alpha*x, y)           = Cosine(x,y)  TRUE!
sim1[0,199]= 0.94328770145935159
sim2[0,199]= 0.94328770145935159

2.- Cosine(beta+x, y)           = Cosine(x,y)  FALSE!
sim1[0,199]= 0.94328770145935159
sim2[0,199]= 0.95462879986489357


EXTENDEDJACCARD
################
XYt = X*Y.T ;  sim1 = XYt / (np.log( np.exp(sum(np.power(X.T,2))).T * np.exp(sum(np.power(Y.T,2))) ) - XYt)
XYt = X*Y.T ;  sim1 = 2*XYt / (np.log( np.exp(sum(np.power(2*X.T,2))).T * np.exp(sum(np.power(Y.T,2))) ) - 2*XYt)
3.- ExtendedJaccard(alpha*x, y)  = ExtendedJaccard(x,y)  FALSE!
sim1[0,14] = 0.64307331710145321
sim2[0,14] = 0.0

4.- ExtendedJaccard(beta+x, y)  = ExtendedJaccard(x,y)  FALSE!
sim2[0,199] = 0.95462879986489357
sim1[0,199] = 0.94328770145935159


CORRELATION
################
X_ = zscore(X,axis=1,ddof=1)
Y_ = zscore(Y,axis=1,ddof=1)
sim1 = (X_*Y_.T)/(M-1)
sim2 = (2*X_*Y_.T)/(M-1)
5.- Correlation(alpha*x, y)      = Correlation(x,y)  TRUE!
sim2[0,14] = -0.10103069562553288
sim1[0,14] = -0.10103069562553288


6.- Correlation(beta+x, y)      = Correlation(x,y)   TRUE!
sim1[0,14] = -0.10103069562553288
sim2[0,14] = -0.10103069562553275

"""

