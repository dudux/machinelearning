# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 22:12:22 2014

Generate 1000 samples from a 2-dimensional normal distribution with mean
μ= [13 17]
and covariance matrix
Σ=[ 4 3
    3 9]

Hints:
Look at the function np.random.multivariate_normal() to learn how you
can generate multivariate normally distributed random numbers in Python.

"""

import numpy as np

# number of samples
N     = 1000
# theoretical 2D-mean
mu    = np.asarray( [13,17])
# covariance 2D-matrix
covmat = np.asmatrix([[4, 3],[ 3, 9]])

X = np.random.multivariate_normal(mu, covmat, N)

print X