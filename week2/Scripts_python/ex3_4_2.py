# -*- coding: utf-8 -*-
"""
Created on Sun Feb 16 22:22:41 2014

Write a script that compares digits generated using a 1-dimensional normal dis-
tribution for each pixel with digits generated using a 16 x 16 = 256-dimensional
multivariate normal distribution (with the mean and covariance of the data).


a) Which model is best? Try changing the analyzed digits and see what happens.

TODO

"""

from pylab import *
import numpy as np
from scipy.io import loadmat


# Load Matlab data file to python dict structure
traindata = loadmat('../../week1/Data/zipdata.mat')['traindata']
X         = mat(traindata[:,1:])
P         = mat(np.zeros((256,256)))

mus = stds = []
for i in xrange(1,257):
    mu = np.mean(traindata[:,i]) # mean
    s  = np.std(traindata[:,i])  # stddev
    mus.append(mu)
    stds.append(s)
    # 1D normal distribution  (256 values)
    P[i-1,:] = np.random.normal(mu,s,256) # 1 x 256



musA  = np.array(mus)
stdsA = np.array(stds)
# MULTIVARIATE   mu=256  covmatr= 256x256
#np.random.multivariate_normal(musA,,256)

''' TODO  Continue '''





