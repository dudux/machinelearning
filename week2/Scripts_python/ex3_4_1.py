# -*- coding: utf-8 -*-
"""
Created on Sun Feb 16 19:53:30 2014

Load the digits data set from last week and compute the mean and the standard
deviation of each digit.

a) Does the mean look like you would expect? 

Yes, pixels on the edges have much lower means than in the middle

b) Can you explain why the standard deviation is highest along the edges of the digit one?

It is not, which is what we expected.

"""

import numpy as np
from scipy.io import loadmat
try:
	import tkinter as tk
except:
	import Tkinter as tk

# Load Matlab data file to python structure
traindata = loadmat('../../week1/Data/zipdata.mat')['traindata']
m = tk.Tk()

w = tk.Canvas(m, width=16, height=16)
w.pack()

print traindata

for i in xrange(1,257):
    x = int((i-1) / 16)
    y = (i-1) % 16
    c = np.std(traindata[:,i]) * 256
    print "Pixel: {0:02d}x{1:02d}\t\t Mean: {2: 2.4f}\t Std.dev: {3: 1.4f}".format(x,y, np.mean(traindata[:,i]), np.std(traindata[:,i]) )
    w.create_line(x,y,x,y,width=1,fill="#%02x%02x%02x" %(c,c,c))

w.mainloop()