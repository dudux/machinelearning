# -*- coding. utf-8 -*-
"""
Created on Thu Feb 13 19.56.10 2014

Calculate the (empirical) mean, variance, median and range of the following set of numbers:
{-0.68,-2.11,2.39,0.26,1.46,1.33,1.03,-0.41,-0.33,0.47}

"""

import numpy as np

set = np.asarray([-0.68,-2.11,2.39,0.26,1.46,1.33,1.03,-0.41,-0.33,0.47])

print "Mean     : %.3f" % np.mean(set)
print "Variance : %.3f" % np.var(set)
print "Median   : %.3f" % np.median(set)
print "Range    : %.3f" % (np.max(set)-np.min(set))
