# -*- coding: utf-8 -*-
"""
Created on Sat Feb 15 16:24:11 2014

Key stuff: empirical mean and standard deviation
    

Compute the empirical mean and standard deviation of the generated samples.
Show, that they are close but not equal to the theoretical values used to generate
the random samples.

Run the code a few times and describe how the empirical mean and standard
deviation changes when new random numbers are generated from the same
distribution.

The difference between the theoretical and empirical mean and standard deviation
themselves are normally distributed random variables.

"""

import numpy as np
import matplotlib.pyplot as plt

# number of samples
N     = 100
# theoretical mean
mu    = 17 
# theoretical standard deviation
sigma = 2 
# Number of bins in histogram
nbins = 15

dom   = []
dos   = []

for x in xrange(1000):
	m     = np.random.normal(mu, sigma, N)

	#print 'Theoretical mean (mu)                  : {0}'.format(mu)
	#print 'Theoretical Standard deviation (sigma) : {0}\n'.format(sigma)
	#print 'Empirical mean (mu)                    : {0:.3f}'.format(m.mean())
	#print 'Empirical standard deviation (sigma)   : {0:.3f}\n'.format(m.std())
	#print 'Difference  between means              : {0:.3f}'.format(abs(mu-m.mean()))
	#print 'Difference  between std deviations     : {0:.3f}'.format(abs(sigma-m.std()))

	dom.append(mu-m.mean())
	dos.append(sigma-m.std())

plt.hist(dom,nbins)
plt.show()

print 'Mean of DoMs                       : {0}'.format(np.mean(dom))
print 'Standard deviation of DoMs         : {0}\n'.format(np.std(dom))

plt.hist(dos,nbins)
plt.show()

print 'Mean of Doss                       : {0}'.format(np.mean(dos))
print 'Standard deviation of DoSs         : {0}\n'.format(np.std(dos))
#plt.hist(m,nbins)
#plt.show()
