# exercise 3.3.3

'''

Inspect and run the script ex3_3_3.py which generates random samples from
the normal distribution and plots the histogram as before. Also, the function
plots the theoretical probability density function (pdf) of the normal distribution
using scipy.stats.norm.pdf().

Show that when the number of samples N is increased, the histogram approx-
imates the pdf better and the empirical estimates of the mean and standard
deviation improve.

Let's modify the number of samples by increasing N variable and we appreciate 
much more accuracy regarding empirical mean and std.dev. Futhermore, the prob. density function(pdf)
is also improving because the Gaussian bell curve is covering almost every bin. 
The more random samples,the more accuracy in the mean and std.dev and pdf. This is basically because there's more 
likehood to equalize

Example:
N = 500
Empirical mean:  16.992103471
Empirical std.dev.:  2.04094376388

N = 10000000
Empirical mean    :  17.0000875557
Empirical std.dev.:  1.99931598611

'''

from pylab import *
import numpy as np
from scipy import stats

# Number of samples
N = 500

# Mean
mu = 17

# Standard deviation
s = 2

# Number of bins in histogram
nbins = 20

# Generate samples from the Normal distribution
X = np.mat(np.random.normal(mu,s,N)).T 
# or equally:
X = np.mat(np.random.randn(N)).T * s + mu

# Plot the histogram
f = figure()
f.hold()
title('Normal distribution')
hist(X, bins=nbins, normed=True)

# Over the histogram, plot the theoretical probability distribution function:
x = linspace(X.min(), X.max(), 1000)
pdf = stats.norm.pdf(x,loc=17,scale=2)
plot(x,pdf,'.',color='red')

# Compute empirical mean and standard deviation
mu_ = X.mean()
s_ = X.std(ddof=1)

print "Theoretical mean: ", mu
print "Theoretical std.dev.: ", s
print "Empirical mean: ", mu_
print "Empirical std.dev.: ", s_

show()
