# exercise 6.3.1
"""
-- Inspect and run the script ex6_3_1.py. The script analyses the body data with
the regularized least squares regression method. In the inner loop of the script
10-fold cross-validation is used to select for the optimal value of regularization
?. In the outer loop 5-fold cross-validation is used to evaluate the performance
of the optimal selected value for ?.

-- What is plotted on each of the generated plots?
Left:  weights as a function of lambda (per fold)
Right: train/test error as a function of lambda (averaged over all 10 folds)

-- How is the optimal value of the regularization (?) chosen?
The value for lambda is chosen from the input lambdas
that yields the lowest mean MSE value over all 10 folds.

-- What do you think may be the advantages and disadvantages of regularized linear regression
compared to the sequential feature selection we used in Exercise 6.2?
Both techniques can prevent overfitting in different ways. They can be used independently.
The regularized linear regression can prevent overfitting when fitting a higher-order polynomial.
It prevents the polynomial to become overly complex in order to fit the data.
However, it does not prevent entering too many features that
do not add independent additional data to the model, also causing overfitting.
The sequential feature selection prevents overfitting to occur in the case of entering too many features.
However, it does not prevent the fitting polynomial itself to become overly complex.
"""

from pylab import *
from scipy.io import loadmat
import sklearn.linear_model as lm
from sklearn import cross_validation
from toolbox_02450 import rlr_validate

mat_data = loadmat('../Data/body.mat')
X = np.matrix(mat_data['X'])
y = np.matrix(mat_data['y'])
attributeNames = [name[0] for name in mat_data['attributeNames'][0]]
N, M = X.shape

# Add offset attribute
X = np.concatenate((np.ones((X.shape[0],1)),X),1)
attributeNames = [u'Offset']+attributeNames
M = M+1

## Crossvalidation
# Create crossvalidation partition for evaluation
K = 5
CV = cross_validation.KFold(N,K)

# Values of lambda
lambdas = np.power(10.,range(-5,9))

# Initialize variables
#T = len(lambdas)
Error_train = np.empty((K,1))
Error_test = np.empty((K,1))
Error_train_rlr = np.empty((K,1))
Error_test_rlr = np.empty((K,1))
Error_train_nofeatures = np.empty((K,1))
Error_test_nofeatures = np.empty((K,1))
w_rlr = np.matrix(np.empty((M,K)))
w_noreg = np.matrix(np.empty((M,K)))

k=0
for train_index, test_index in CV:
    
    # extract training and test set for current CV fold
    X_train = X[train_index]
    y_train = y[train_index]
    X_test = X[test_index]
    y_test = y[test_index]
    internal_cross_validation = 10    
    
    opt_val_err, opt_lambda, mean_w_vs_lambda, train_err_vs_lambda, test_err_vs_lambda = rlr_validate(X_train, y_train, lambdas, internal_cross_validation)

    Xty = X_train.T*y_train
    XtX = X_train.T*X_train
    
    # Compute mean squared error without using the input data at all
    Error_train_nofeatures[k] = np.square(y_train-y_train.mean()).sum()/y_train.shape[0]
    Error_test_nofeatures[k] = np.square(y_test-y_test.mean()).sum()/y_test.shape[0]

    # Estimate weights for the optimal value of lambda, on entire training set
    w_rlr[:,k] = linalg.lstsq(XtX+opt_lambda*np.eye(M),Xty)[0]
    # Compute mean squared error with regularization with optimal lambda
    Error_train_rlr[k] = np.square(y_train-X_train*w_rlr[:,k]).sum()/y_train.shape[0]
    Error_test_rlr[k] = np.square(y_test-X_test*w_rlr[:,k]).sum()/y_test.shape[0]

    # Estimate weights for unregularized linear regression, on entire training set
    w_noreg[:,k] = linalg.lstsq(XtX,Xty)[0]
    # Compute mean squared error without regularization
    Error_train[k] = np.square(y_train-X_train*w_noreg[:,k]).sum()/y_train.shape[0]
    Error_test[k] = np.square(y_test-X_test*w_noreg[:,k]).sum()/y_test.shape[0]
    # OR ALTERNATIVELY: you can use sklearn.linear_model module for linear regression:
    #m = lm.LinearRegression().fit(X_train, y_train)
    #Error_train[k] = np.square(y_train-m.predict(X_train)).sum()/y_train.shape[0]
    #Error_test[k] = np.square(y_test-m.predict(X_test)).sum()/y_test.shape[0]

    figure(k, figsize=(12,8))
    subplot(1,2,1)
    semilogx(lambdas,mean_w_vs_lambda.T,'.-')
    xlabel('Regularization factor')
    ylabel('Mean Coefficient Values')    
    
    subplot(1,2,2)
    title('Optimal lambda = {0}'.format(opt_lambda))
    loglog(lambdas,train_err_vs_lambda.T,'b.-',lambdas,test_err_vs_lambda.T,'r.-')
    xlabel('Regularization factor')
    ylabel('Squared error (crossvalidation)')
    legend(['Train error','Validation error'])
    
    print('Cross validation fold {0}/{1}:'.format(k+1,K))
    print('Train indices: {0}'.format(train_index))
    print('Test indices: {0}\n'.format(test_index))

    k+=1

# Display results
print('\n')
print('Linear regression without feature selection:\n')
print('- Training error: {0}'.format(Error_train.mean()))
print('- Test error:     {0}'.format(Error_test.mean()))
print('- R^2 train:     {0}'.format((Error_train_nofeatures.sum()-Error_train.sum())/Error_train_nofeatures.sum()))
print('- R^2 test:     {0}\n'.format((Error_test_nofeatures.sum()-Error_test.sum())/Error_test_nofeatures.sum()))
print('Regularized Linear regression:')
print('- Training error: {0}'.format(Error_train_rlr.mean()))
print('- Test error:     {0}'.format(Error_test_rlr.mean()))
print('- R^2 train:     {0}'.format((Error_train_nofeatures.sum()-Error_train_rlr.sum())/Error_train_nofeatures.sum()))
print('- R^2 test:     {0}\n'.format((Error_test_nofeatures.sum()-Error_test_rlr.sum())/Error_test_nofeatures.sum()))

show()