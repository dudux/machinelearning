# exercise 6.4.1
"""

Load the wine2.mat file with wine data using the loadmat() function. Use
10-fold cross-validation to evaluate the classification error rate for
1) a logistic regression model and
2) an un-pruned decision tree with the same settings as in Exercise 6.1.1.

Show that for this classification problem, logistic regression outperforms the
decision tree. Try to run the 10-fold cross-validation with random sampling
several times, and in every test use paired t-test to check the significance of
difference between the two classifiers (p-value).


Logistic regression actually does not seem to outperform the decision tree.
Also the p-values we found were never close to .05, hence the difference is insignificant.

"""

from pylab import *
import scipy.io
from scipy.stats import ttest_ind
from sklearn.cross_validation import KFold
from sklearn.linear_model import LogisticRegression
from sklearn import tree
import numpy as np

mat_data = scipy.io.loadmat('../Data/wine2.mat')

X               = mat_data['X']
y               = mat_data['y']
attributeNames  = [name[0] for name in mat_data['attributeNames'][0]]

lre = []
dte = []

kf = KFold(len(y), 10, shuffle=True)
for train_index, test_index in kf:
    Xtrain, Xtest = X[train_index], X[test_index]
    ytrain, ytest = y[train_index], y[test_index]

    # logistic regression
    lr = LogisticRegression(fit_intercept=True)
    lr.fit(Xtrain, ytrain)

    # decision tree
    dt = tree.DecisionTreeClassifier()
    dt.fit(Xtrain, ytrain)

    # put errors for both models in arrays
    lre.append(np.abs(ytest - lr.predict(Xtest)).sum()/ytest.shape[0])
    dte.append(np.abs(ytest - dt.predict(Xtest)).sum()/ytest.shape[0])

p = ttest_ind(lre, dte, equal_var=False)[1]

print 'Logistic regression mean error : {0}'.format(np.mean(lre))
print 'Decision tree mean error       : {0}\n'.format(np.mean(dte))

print 'p-value                        : {0}'.format(p)