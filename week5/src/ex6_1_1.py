# exercise 6.1.1
"""

-- What appears to be the optimal tree depth? 
In this particular example, 12.

-- Do you get the same result when you run your code again, generating a new random split between training and test data?
No, when removing the random_state=42 parameter, the optimal tree depth fluctuates heavily.

-- What other parameters of the tree could you optimize in cross-validation?
criterion, max_features, min_samples_split and min_samples_leaf


"""

from pylab import *
import scipy.io
from sklearn.cross_validation import train_test_split
from sklearn import tree
import numpy as np

wine2 = scipy.io.loadmat('../Data/wine2.mat')

Xtrain, Xtest, ytrain, ytest = train_test_split(
    wine2['X'], wine2['y'], test_size=.25, random_state=42)

S = []
T = []
D = range(2,21)

for d in D:
    tr = tree.DecisionTreeClassifier(max_depth=d)
    tr.fit(Xtrain, ytrain)
    S.append(np.abs(ytest - tr.predict(Xtest)).sum()/ytest.shape[0])#tr.score(Xtest, ytest))
    T.append(np.abs(ytrain - tr.predict(Xtrain)).sum()/ytrain.shape[0])#tr.score(Xtrain, ytrain))

plot(D, S, 'b.-')
#plot(D, T, 'r.-')
show()