# exercise 6.1.2
"""

-- What appears to be the optimal tree depth?
13 - the error seems not to decline significantly when the depth increases

-- Do you get the same result when you run your code again, generating a new random split between training and test
data?
When leaving out the random_state=42 parameter, this is sometimes not the case.
However, the result is much more stable than the result of the last exercise.

-- How about 100-fold cross-validation?
The result seems to be stable, 11. CAUTION: took over 10min of cpu time to run.

-- Leave-one-out cross-validation?
The score always yields 1.0 in the case of a leave-one-out validation.
Therefore we did not include it


"""

from pylab import *
import scipy.io
from sklearn.cross_validation import KFold, LeaveOneOut
from sklearn import tree
import numpy as np

wine2 = scipy.io.loadmat('../Data/wine2.mat')

n = 10
S = [0] * 19
T = [0] * 19
#U = [0] * 19
D = range(2,21)

kf = KFold(len(wine2['y']), n, shuffle=True, random_state=42)
for train_index, test_index in kf:
    Xtrain, Xtest = wine2['X'][train_index], wine2['X'][test_index]
    ytrain, ytest = wine2['y'][train_index], wine2['y'][test_index]

    for d in D:
        tr = tree.DecisionTreeClassifier(max_depth=d)
        tr.fit(Xtrain, ytrain)
        S[d-2] += np.abs(ytest - tr.predict(Xtest)).sum()/ytest.shape[0]
        T[d-2] += np.abs(ytrain - tr.predict(Xtrain)).sum()/ytrain.shape[0]
        #S[d-2] += tr.score(Xtest, ytest)
        #T[d-2] += tr.score(Xtrain, ytrain)

#loo = LeaveOneOut(len(wine2['y']))
#for train_index, test_index in loo:
    #Xtrain, Xtest = wine2['X'][train_index], wine2['X'][test_index]
    #ytrain, ytest = wine2['y'][train_index], wine2['y'][test_index]

    #for d in D:
        #tr = tree.DecisionTreeClassifier(max_depth=d)
        #tr.fit(Xtrain, ytrain)
        ##U[d-2] += tr.score(Xtest, ytest)
        #U[d-2] += np.abs(ytest - tr.predict(Xtest)).sum()/ytest.shape[0]

for d in D:
    S[d-2] /= n
    T[d-2] /= n
    #U[d-2] /= len(wine2['y'])

plot(D, S, 'b.-')
#plot(D, T, 'r.-')
#plot(D, U, 'g.-')

show()