# exercise 6.2.1
"""

-- Using 5-fold cross-validation, fit a linear regression model to estimate the body-
weight based on the attributes. When fitting the models, you should compare
two methods:
  1) using all 23 attributes, and
  2) using 10-fold cross-validation to perform sequential feature selection,
     thus choosing a subset of the 23 attributes.
Refer to the code below

-- Compute the 5-fold cross-validated training and test error with and without
sequential feature selection. Show that without feature selection, the model
overfits.
The average absolute error is much lower for the input filtered by the feature selection algorithm,
except in the case when applying the model on the train data: the opposite holds.
Stated differently, less features are input causes the test set to be predicted
more accurately -- hence, overfitting is clearly the issue.

-- Show that the feature selection tends to choose features such as height
and waist girth, and disregard features such as the wrist diameter, which seems
reasonable when predicting body-weight.
The selected features are printed for each iteration.
Features such as height and waist girth are clearly prefered.

"""

from pylab import *
import scipy.io
from sklearn.cross_validation import KFold
from sklearn.linear_model import LinearRegression
from sklearn import tree
import numpy as np
from toolbox_02450 import feature_selector_lr


mat_data = scipy.io.loadmat('../Data/body.mat')

X               = mat_data['X']
y               = mat_data['y']
attributeNames  = [name[0] for name in mat_data['attributeNames'][0]]

s,t,m,o = 0, 0, 0, 0

i = 1
kf = KFold(len(y), 5, shuffle=True, random_state=42)
for train_index, test_index in kf:
    Xtrain, Xtest = X[train_index], X[test_index]
    ytrain, ytest = y[train_index], y[test_index]

    lr = LinearRegression(fit_intercept=True)
    lr.fit(Xtrain, ytrain)
    
    print '============= iteration {0} ==============='.format(i)
    
    u = np.square(ytest-lr.predict(Xtest)).sum()/ytest.shape[0]
    #u = lr.score(Xtest, ytest)
    v = np.square(ytrain-lr.predict(Xtrain)).sum()/ytrain.shape[0]
    #v = lr.score(Xtrain, ytrain)
    
    print '  unselected train MSE : {0}\n  unselected test MSE  : {1}'.format(v, u)

    s += u
    t += v

    selected_features, features_record, loss_record = feature_selector_lr(Xtrain, ytrain, cvf=10)
    print '  selected features      : {0}'.format([str(attributeNames[j]) for j in selected_features])

    Xftrain = Xtrain[:,selected_features]
    Xftest  = Xtest[:,selected_features]

    lr.fit(Xftrain, ytrain)

    p = np.square(ytest-lr.predict(Xftest)).sum()/ytest.shape[0]
    #p = lr.score(Xftest, ytest)
    q = np.square(ytrain-lr.predict(Xftrain)).sum()/ytrain.shape[0]
    #q = lr.score(Xftrain, ytrain)

    print '  selected train MSE   : {0}\n  selected test MSE    : {1}'.format(q, p)

    m += p
    o += q

    i += 1

s /= 5
t /= 5
m /= 5
o /= 5

print 'average unselected test MSE  : {0}\naverage unselected train MSE : {1}'.format(s, t)
print 'average selected test MSE    : {0}\naverage selected train MSE   : {1}'.format(m, o)