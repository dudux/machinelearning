# -*- coding: utf-8 -*-
"""
Change the value of K and show that reconstruction accuracy improves when more principal components are used.

*) How many principal components do you need to be able to see the different digits properly?

In this case, having 50% of likehood because we only have binary format [0,1]. With a few PC can be quite confident what the 
right digit has been found out. If we take a look at the graph representing just PC1 and PC2, we rapidly note a big group of data
between [-10,-5] being those 1's and on the other hand mostly of the other data is between [5,10] being those 0's.
Even if we try just with two PC as it uses, we can also "recognize" mostly of the sequence.


*) What happens if you set K=256?

In this case we are not reducing any dimension , hence extra work and much more computation is done.
We also appreciate as original is equal to reconstruction due to we do not reduce data.

"""

