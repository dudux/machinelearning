# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 17:39:01 2014

@author: dudu
"""

import numpy as np

m = np.matrix('1 2 3; 4 5 6; 7 8 9')
m[0,0] # first element
m[-1,-1] # last element
m[:,0] # first column
m[0:2,-1] # view on selected rows & columns
