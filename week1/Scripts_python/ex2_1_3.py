# -*- coding: utf-8 -*-
"""
Created on Sun Feb  9 20:49:46 2014

@author: Eduardo Novella & Carlo Meijer

Key stuff: Stopwords

"""

from tmgsimple import TmgSimple

tm = TmgSimple('../Data/textDocs.txt','../Data/stopWords.txt')
l  = tm.get_words(sort=True) # or to make sure on just unique words l  = set(tm.get_words(sort=True))
X  = tm.get_matrix(sort=True)
print l
print
print X


''' How does it compare to your original matrix?
In our first matrix, we only picked 2 key words for every document. 
In this way, we are working with all the words in the document, and we are applying stemming and 
the use of stopwords to focus really where right data is important, like unique words.
'''
