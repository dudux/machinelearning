# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 17:16:14 2014

QC--- 2.3.3 Inspect and run the script ex2_3_2.py. 

*)Show that it requires 22 PCA components to account for more than 90% of the variance in the data. 

We can prove it by using the squared singular value divided by the sum of all squared singular values (rho) 
until the 22th component than is larger than 90%
rho[0:22].sum()
Out[108]: 0.90372941988453037


*)Show that the first principal component is almost sufficient to separate zeros and ones. Exam-
ine the first principal component and discuss and understand what it captures.

The PC1 is capturing 1's and we can observe as all values in this component are negatives. That does easy the recognition.
Z[class_mask,0].max() =-2.6318160244200151
Z[class_mask,0].min() = -8.5638600099290887

Z[class_mask,1].min() = -5.2705820312509717
Z[class_mask,1].max() = 1.4860342713536903


"""

# exercise 2.3.2

from pylab import *
import scipy.linalg as linalg
from scipy.io import loadmat

# Digits to include in analysis (to include all, n = range(10) )
n = [0,1]
# Number of principal components for reconstruction
K = 16
# Digits to visualize
nD = range(6);


# Load Matlab data file to python dict structure
# and extract variables of interest
traindata = loadmat('../Data/zipdata.mat')['traindata']
X         = mat(traindata[:,1:])
y         = mat(traindata[:,0]).T

N,M = X.shape
C   = len(n)

classValues = unique(y.A)
classNames  = [str(int(v)) for v in classValues]
classDict   = dict(zip(classNames,classValues))


# Select subset of digits classes to be inspected
class_mask = np.zeros(N).astype(bool)
for v in n:
    cmsk = y.A.ravel()==v
    class_mask = class_mask | cmsk
X = X[class_mask,:]
y = y[class_mask,:]
N=X.shape[0]

# Center the data (subtract mean column values)
Xc = X - np.ones((N,1))*X.mean(0)

# PCA by computing SVD of Y
U,S,V = linalg.svd(Xc,full_matrices=False)
U = mat(U)
V = mat(V).T

# Compute variance explained by principal components
rho = (S*S) / (S*S).sum() 

# Project data onto principal component space
Z = Xc * V

# Plot variance explained
figure()
plot(rho,'o-')
title('Variance explained by principal components');
xlabel('Principal component');
ylabel('Variance explained value');


# Plot PCA of the data
f = figure()
f.hold()
title('NanoNose data: PCA')
for c in n:
    # select indices belonging to class c:
    class_mask = y.A.ravel()==c
    plot(Z[class_mask,0], Z[class_mask,1], 'o')
legend(classNames)
xlabel('PC1')
ylabel('PC2')


# Visualize the reconstructed data from the first K principal components
# Select randomly D digits.
figure()
W = Z[:,range(K)] * V[:,range(K)].T
D = len(nD)
for d in range(D):
    digit_ix = np.random.randint(0,N)
    subplot(2, D, d+1)
    I = reshape(X[digit_ix,:], (16,16))
    imshow(I, cmap=cm.gray_r)
    title('Original')
    subplot(2, D, D+d+1)
    I = reshape(W[digit_ix,:]+X.mean(0), (16,16))
    imshow(I, cmap=cm.gray_r)
    title('Reconstr.');
    

# Visualize the pricipal components
figure()
for k in range(K):
    N1 = ceil(sqrt(K)); N2 = ceil(K/N1)
    subplot(N2, N1, k+1)
    I = reshape(V[:,k], (16,16))
    imshow(I, cmap=cm.hot)
    title('PC{0}'.format(k+1));

# output to screen
show()
