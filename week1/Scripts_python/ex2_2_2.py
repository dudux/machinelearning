# -*- coding: utf-8 -*-
'''
Created on Mon Feb 10 02:20:34 2014

@author  : Eduardo Novella & Carlo Meijer
Key stuff: Scatter plot

The data resides in an 8 dimensional space where each dimension corresponds
to each of the 8 NanoNose sensors. This makes visualization of the raw data
difficult, because it is difficult to plot data in more than 2 or 3 dimensions.
Plot the two attributes A and B against each other in a scatter plot.
'''


import xlrd
import numpy as np
from pylab import *

wb     = xlrd.open_workbook('../Data/nanonose.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i)[3:])

X = np.matrix(l).reshape(90,8)


''' Plotting '''
A = X[:,0] # A column
B = X[:,1] # B column
xlabel('A values')
ylabel('B values')

# normal plot
#plot(A,'.-r')
#plot(B,'.-b')

# Scatter plot
#pA = plot(A,'o')
#pB = plot(B,'o')

plot(A,B,'o')

title('A and B values for nanonose.xls file')

show()


