# -*- coding: utf-8 -*-
'''
Created on Sun Feb  9 23:00:28 2014

@author: Eduardo Novella & Carlo Meijer
Key stuff: measures of similarity && cosine distance || cosine similarity

cos(q,Xi) = q/||q||  * Xi/||Xi|| = qXi.T/(||q||*||Xi||)
'''

from tmgsimple import TmgSimple
import numpy as np


'''the query 'solving for the rank of a matrix' 
                             matrix                  rank solv
 [ 0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  0.  0.  0.  1.  1.  0.  0.  0.]
 '''
# Let's create a query matrix with numpy
query = [ 0,0,0,0,0,0,0,1,0,0,0,0,0,1,1,0,0,0 ]
q = np.matrix(query)


def cosineSimilarity(x):
    qxit   = np.dot(q,x.T) # or equivalent to  q*x.T
    norm_q = np.linalg.norm(q)
    norm_x = np.linalg.norm(x)
    return qxit/(norm_q*norm_x)
    

tm = TmgSimple('../Data/textDocs.txt','../Data/stopWords.txt',True)
l  = tm.get_words(sort=True) # or to make sure on just unique words l  = set(tm.get_words(sort=True))
X  = tm.get_matrix(sort=True)

print '*'*100
print l
print
print X
print '*'*100
print 

max = 0
for i,row in enumerate(X):
    # extract a document (row)
    x = X[i,:]
    r = cosineSimilarity(x)
    print 'Document{0} ==> Cosine similarity: {1}'.format(i+1,r)
   
    if (r>max):
        max = r
        doc = i+1
    

print '\nThe document has to be: {0} with this rate {1}'.format(doc,max)


'''  QC1 = Explain what documents, according to our similarity measure, are most related to the query.
     After calculating the cosine distance,we can appreciate the highest value close to 1 as the Document4, what indeed
     is the file which is containing the 3 words in it. We can also appreciate like the Document3 with 0.5163... is relatively
     nearby to 0.7745... due to it has 2 matches instead 3 matches.
'''
