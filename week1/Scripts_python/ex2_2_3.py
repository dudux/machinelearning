# -*- coding: utf-8 -*-
'''
Created on Mon Feb 10 02:20:34 2014

@author  : Eduardo Novella & Carlo Meijer
Key stuff: PCA && SVD

The data resides in an 8 dimensional space where each dimension corresponds
to each of the 8 NanoNose sensors. This makes visualization of the raw data
difficult, because it is difficult to plot data in more than 2 or 3 dimensions.
Plot the two attributes A and B against each other in a scatter plot.
'''


import xlrd
import numpy as np
from pylab import *
#from sklearn.decomposition import PCA

wb     = xlrd.open_workbook('../Data/nanonose.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i)[3:])

X = np.matrix(l).reshape(90,8)

# X.mean(0) is giving us the same that this loop
    #for i in xrange(8):
    #    print X[:,i].mean()

# Center the data (subtract mean column values)
# Y = X -1u  
Y =  X - np.ones((90,1))*X.mean(0)

# PCA by computing SVD of Y
# Y = UVS^T
U,S,V = linalg.svd(Y,full_matrices=False)
U     = mat(U)
V     = mat(V).T

# Compute variance explained by principal components
# the squared singular value of the given component divided by the sum of all the squared singular values.
rho = (S*S) / (S*S).sum() 

# Project data onto principal component space
Z = Y * V

print "Data explained of first 3 dimensions : {0}".format(rho[0:3].sum()) # = 0.92745

# Plot variance explained
figure()
plot(rho,'o-')
title('Variance explained by principal components');
xlabel('Principal component');
ylabel('Variance explained value');
show()


''' QC1: Can you verify that more than 90% of the variation in the data is explained by
the first 3 principal components?

Yes, the sum of the variation of the first three components equals 0.92745 (line 50).
Hence, the total variation of the first three components exceeds 90%.

'''