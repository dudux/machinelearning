# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 16:06:36 2014

@author: dudu
"""

import numpy as np

''' Multiply arrays'''
a = np.random.rand(2,2)
b = np.array( [[2,0],[0,2]])

print a
print b
print a*b

print "#"*50
''' convert to matrices and multiply '''
a = np.asmatrix(a)
b = np.asmatrix(b)
np.asmatrix

print a
print b
print a*b
