# -*- coding: utf-8 -*-
"""
Created on Fri Feb  7 02:16:13 2014

@author: Eduardo Novella & Carlo Meijer
"""

from tmgsimple import TmgSimple

tm = TmgSimple('../Data/textDocs.txt')
l  = tm.get_words(sort=True) # or to make sure on just unique words l  = set(tm.get_words(sort=True))
X  = tm.get_matrix(sort=True)
print l
print
print X


# Some words like : the,there,out,from are stopwords and they should be removed because
# they do not distinguish documents


