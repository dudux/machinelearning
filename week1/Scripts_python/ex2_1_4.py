# -*- coding: utf-8 -*-
"""
Created on Sun Feb  9 21:28:19 2014

@author: Eduardo Novella & Carlo Meijer
Key stuff: Stopwords && stemming
"""

from tmgsimple import TmgSimple

tm = TmgSimple('../Data/textDocs.txt','../Data/stopWords.txt',True)
l  = tm.get_words(sort=True) # or to make sure on just unique words l  = set(tm.get_words(sort=True))
X  = tm.get_matrix(sort=True)
print l
print
print X

''' >>>How does it compare to your original matrix?
Much more abstraction. We are able to distinguish quickly.
For example, the query 'solving for the rank of a matrix' can be matched with 
4th row due 'solv' is only there. Therefore, the document4 is the target.


['drop', 'eigenvalu', 'england', 'fifa', 'googl', 'internet', 'link', 'matrix', 
 'model', 'nonzero', 'p_ij', 'page', 'problem', 'rank', 'solv', 'top', 'web', 'webpag']

[[ 0.  0.  0.  0.  1.  1.  0.  1.  1.  0.  0.  0.  0.  0.  0.  0.  0.  0.]
 [ 0.  0.  0.  0.  0.  0.  1.  0.  0.  1.  1.  0.  0.  0.  0.  0.  0.  1.]
 [ 0.  0.  0.  0.  1.  0.  0.  1.  0.  0.  0.  1.  0.  1.  0.  0.  1.  0.]
 [ 0.  1.  0.  0.  0.  0.  0.  1.  0.  0.  0.  0.  1.  1.  1.  0.  0.  0.]
 [ 1.  0.  1.  1.  0.  0.  0.  0.  0.  0.  0.  0.  0.  1.  0.  1.  0.  0.]]

                             matrix                  rank solv
 [ 0.  0.  0.  0.  0.  0.  0.  1.  0.  0.  0.  0.  0.  1.  1.  0.  0.  0.]

'''
