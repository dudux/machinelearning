# -*- coding: utf-8 -*-
"""
@author: Eduardo Novella & Carlo Meijer

Propose a suitable bag of words representation for these documents. 
You should choose approximately 10 key words in total defining the 
columns in the document-term matrix and the words are to be chosen such that 
each document at least contains 2 of your key words, i.e. the document-term 
matrix should have approximately 10 columns and each row of the matrix must
 at least contain 2 non-zero entries.
 
Document 1: The Google matrix  P is a model of the internet
Document 2: P_ij is nonzero if there is a link from webpage i to j
Document 3: The Google matrix is used to rank all Web pages.
Document 4: The ranking is done by solving a matrix eigenvalue problem.
Document 5: England dropped out of the top 10 in the FIFA ranking.


Solution:
We can offer this representation as bag-of-words representation. 
It could also be used another combination instead of using the word 'matrix'.
In this case we will have a M with only 2 non-zero elements.

       matrix   model     nonzero webpage     matrix  rank     matrix   eigenvalue   England FIFA
Doc1     1        1        0        0            1     0         1         0            0     0
Doc2     0        0        1        1            0     0         0         0            0     0
Doc3     1        0        0        0            1     1         1         0            0     0
Doc4     1        0        0        0            1     0         1         1            0     0
Doc5     0        0        0        0            0     0         0         0            1     1   


"""

