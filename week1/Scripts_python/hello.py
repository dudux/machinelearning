# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
/home/dudu/.spyder2/.temp.py
"""

import numpy as np
def hello(name, n):
    M = np.random.rand(n,n)
    M = np.asmatrix(M)
    print('\nHello {0}! This is your matrix:\n{1}'.format(name, M))

#hello('Edu',3)
