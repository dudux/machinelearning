# -*- coding: utf-8 -*-
"""

Try decomposing one digit at a time by modifying the variable n to contain only
a single digit. 

*)Explain what happens to the principal components when only a single digit type is 
analyzed compared to when all digit types are analyzed at the same time.

If we analise only a digit, we can observe as the graph with PCs does not make sense with
different K's. Always we obtain the same graph and we also appreciate as the values are not too
dispersed like before. The first PC is dispersed in both dimensions and it does not note any relevant
change.

"""

