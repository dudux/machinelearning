# -*- coding: utf-8 -*-
"""
Created on Thu Feb  6 17:31:03 2014

@author: dudu
"""

import numpy as np

##Creating arrays a1-a8 and matrices m1-m3
a1 = np.array([[1, 2, 3], [4, 5, 6]]) # define explicitly
a2 = np.arange(1,7).reshape(2,3) # reshape range of numbers
a3 = np.zeros([3,3]) # zeros array
a4 = np.eye(3) # diagonal array
a5 = np.random.rand(2,3) # random array
a6 = a1.copy() # copy
a7 = a1 # alias
m1 = np.matrix('1 2 3; 4 5 6; 7 8 9') # define matrix by string
m2 = np.asmatrix(a1.copy()) # copy array into matrix
m3 = np.mat(np.array([1, 2, 3])) # map array onto matrix
a8 = np.asarray(m1) # map matrix onto array
