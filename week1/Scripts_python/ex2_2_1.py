# -*- coding: utf-8 -*-
"""
Created on Sun Feb  9 23:56:55 2014

@author: Eduardo Novella & Carlo Meijer
Key stuff: Import excel files in python


There are 90 data objects with 8 attributes each. Can you get the correct data matrix X of size 90x 8?
We can read the rows and extract a list for each row. Concatenate all lists in other list and later create
a numpy matrix with this list a reshape the structure
"""

import xlrd
import numpy as np

wb     = xlrd.open_workbook('../Data/nanonose.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i)[3:])

X = np.matrix(l).reshape(90,8)

print X



#num_rows  = sheet.nrows - 1
#curr_row  = 1  # Skip headers
#while curr_row < num_rows:
#     curr_row += 1
#     row = sheet.row(curr_row)
#     print row