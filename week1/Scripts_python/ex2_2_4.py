# -*- coding: utf-8 -*-
'''
Created on Mon Feb 10 02:20:34 2014

@author  : Eduardo Novella & Carlo Meijer
Key stuff: PCA && SVD



Plot principal component 1 and 2 against each other in a scatterplot.



QC1-- What are the benefits of visualizing the data by the projection given by PCA
over plotting two of the original data dimensions against each other? Compare
with the scatter plots of attributes you made in Exercise 2.2.2.

Reducing dimensions by means of PCA enables us to represent the same data in a smaller number of dimensions.
The transformation is defined in such a way that the first principal components has the largest possible variation.
Hence, taking a smaller number of components than the number of dimensions in the original data set
compresses the data such that only the components with the least amount of variation are taken out.
We can use this to make 2 or 3 dimensional plots of data of higher dimensionality, without losing of most of the variation.
Also, taking only the components with the least variation can be used to prevent overfitting a model.

When comparing the plots of exercise 2.2.2 and 2.2.4 we observe that more variation exists in the plot generated
in exercise 2.2.4, which is to be expected.

'''

import xlrd
import numpy as np
from pylab import *
#from sklearn.decomposition import PCA

wb     = xlrd.open_workbook('../Data/nanonose.xls')
sheet  = wb.sheet_by_name('Sheet1')  # or ...... sheet = wb.sheet_by_index(0)
     
l = []     
for i in xrange(2,sheet.nrows):
    l.append(sheet.row_values(i)[3:])

X = np.matrix(l).reshape(90,8)

# Center the data (subtract mean column values)
# Y = X -1u  
Y =  X - np.ones((90,1))*X.mean(0)

# PCA by computing SVD of Y
# Y = UVS^T
U,S,V = linalg.svd(Y,full_matrices=False)
U     = mat(U)
V     = mat(V).T

# Compute variance explained by principal components
# the squared singular value of the given component divided by the sum of all the squared singular values.
rho = (S*S) / (S*S).sum() 

# Project data onto principal component space
Z = Y * V

# Plotting
A = Z[:,0] # A column
B = Z[:,1] # B column
xlabel('First component values')
ylabel('Second component values')


# Scatter plot
#plot(A,'o',label=r'$A$')
#plot(B,'o',label=r'$B$')
plot(A,B,'o',label=r'$A-B$')
#legend(loc='upper right')

title('Component 1 & 2 for Nanonose.xls file')

show()