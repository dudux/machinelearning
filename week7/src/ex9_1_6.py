# exercise 9_1_6
"""
Repeat the exercise with the digits data set. Load the digits data set from
load Data/digits. Each data object is a 16 x 16 = 256 dimensional vector,
corresponding to a gray scale 16 x 16 pixels image.
Hints:
- You can change the color map to black-on-white grey-scale by adding the
parameter cmap=cm.binary to the function imshow().
- If you are stuck, take a look at the solution in ex9_1_5.py. It can easily be
modified to work for the digits data set as well.

-- Why does running k-means with K = 10 not give you 10 clusters corresponding to the 10 digits 0-9?
The clustering algorithm attempts to divide the given data set into several
non-overlapping groups (clusters). Each cluster consists of objects that are similar
between themselves and dissimilar to objects of other clusters.
Therefore, since digits typically have a number of overlapping features,
the clustering algorithm will not be able to yield 10 clusters corresponding to the digits 0-9.

-- How many clusters do you need to visually represent the 10 different digits?
K = 50 seems to work well (by trial and error)

-- Are there any digits that the clustering algorithm seems to confuse more than others?
Trivially, similar digits are confused with each other more than digits with more unique features.
"""
from pylab import *
from scipy.io import loadmat
from sklearn.cluster import k_means
#from toolbox_02450 import clusterplot


# Load Matlab data file and extract variables of interest
mat_data = loadmat('../Data/digits.mat')
X = np.matrix(mat_data['X'])
N, M = X.shape
# Image resolution and number of colors
x = 16 
y = 16


# Number of clusters:
K = 10

# Number of repetitions with different initial centroid seeds
S = 1

# Run k-means clustering:
centroids, cls, inertia = k_means(X, K, verbose=True, max_iter=100, n_init=S)

# Plot results:

# Plot centroids
figure(1)
n1 = np.ceil(np.sqrt(K/2)); n2 = np.ceil(np.float(K)/n1);
for k in range(K):
    subplot(n1,n2,k+1)
    imshow(np.reshape(centroids[k,:],(x,y)).T,interpolation='None',cmap=cm.binary)
    xticks([]); yticks([])
    if k==np.floor((n2-1)/2): title('Centroids')

# Plot few randomly selected digits and their nearest centroids    
L = 10       # number of images to plot
j = np.random.randint(0, N, L)
figure(2)
for l in range(L):
    subplot(2,L,l+1)
    imshow(np.resize(X[j[l],:],(x,y)).T,interpolation='None',cmap=cm.binary)
    xticks([]); yticks([])
    if l==np.floor((L-1)/2): title('Randomly selected digits and their centroids')
    subplot(2,L,L+l+1)
    imshow(np.resize(centroids[cls[j[l]],:],(x,y)).T,interpolation='None',cmap=cm.binary)
    xticks([]); yticks([])

#figure(3)
#title('Cluster plot')
#hold(True)
#clusterplot(X, cls, centroids, mat_data['y'])

show()