# exercise 9_1_5
"""
We will consider a subset of the wild faces data described in [2]. Load the
wildfaces data, Data/wildfaces using the loadmat function. Each data object
is a 40 x 40 x 3 = 4800 dimensional vector, corresponding to a 3-color 40 x 40
pixels image. Compute a k-means clustering of the data with K = 10 clusters.

Plot a few random images from the data set as well as their corresponding cluster
centroids to see how they are represented.
Hints:
- You can plot an image by the command imshow(np.reshape(X[k,:],(c,x,y)).T)
which reshapes an image vector to a 3-dimensional array and plots it. Similarly,
you can plot the cluster centroids.
- Running k-means on a large data set can be slow. If you type
k_means(X, K, verbose=True, max_iter=X, n_init=S) it will provide
information about the iterations as they run. n_init as before constrains
the number of initial repeated centroid seeds. Type help(kmeans) to read
more about these options.
- If you are stuck, take a look at the solution in ex9_1_5.py.

-- How well is the data represented by the cluster centroids?
Quite well. Faces seem to be quite suitable to be represented by cluster centroids.

-- Are you able to recognize the faces in the compressed representation?
It varies but some are recognizable quite well.

-- What happens if you increase or decrease the number of clusters?
Increasing the number of clusters yields a sharper image (less compression) and vice-versa.
"""
from pylab import *
from scipy.io import loadmat
from sklearn.cluster import k_means


# Load Matlab data file and extract variables of interest
mat_data = loadmat('../Data/wildfaces.mat')
X = np.matrix(mat_data['X'])
N, M = X.shape
# Image resolution and number of colors
x = 40 
y = 40
c = 3


# Number of clusters:
K = 10

# Number of repetitions with different initial centroid seeds
S = 1

# Run k-means clustering:
centroids, cls, inertia = k_means(X, K, verbose=True, max_iter=100, n_init=S)


# Plot results:

# Plot centroids
figure(1)
n1 = np.ceil(np.sqrt(K/2)); n2 = np.ceil(np.float(K)/n1);
for k in range(K):
    subplot(n1,n2,k+1)
    imshow(np.reshape(centroids[k,:],(c,x,y)).T,interpolation='None',cmap=cm.binary)
    xticks([]); yticks([])
    if k==np.floor((n2-1)/2): title('Centroids')

# Plot few randomly selected faces and their nearest centroids    
L = 5       # number of images to plot
j = np.random.randint(0, N, L)
figure(2)
for l in range(L):
    subplot(2,L,l+1)
    imshow(np.resize(X[j[l],:],(c,x,y)).T,interpolation='None',cmap=cm.binary)
    xticks([]); yticks([])
    if l==np.floor((L-1)/2): title('Randomly selected faces and their centroids')
    subplot(2,L,L+l+1)
    imshow(np.resize(centroids[cls[j[l]],:],(c,x,y)).T,interpolation='None',cmap=cm.binary)
    xticks([]); yticks([])

show()