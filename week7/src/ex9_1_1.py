# exercise 9.1.1
"""
Load the Data/synth1 data into Python using the loadmat function. Cluster
the data into K = 4 clusters using the k-means algorithm. Make a scatter plot
of the data and the clustering using the clusterplot() function in the toolbox.

Hints:
- In Python, you can use the function k_means() from the package sklearn.cluster
to compute k-means clustering. Import the function and type help(k_means)
to learn how to use the function.
- The function can be called as centroids, clusters, inertia = k_means(X,K);
where K is the number of clusters.
- Type clusterplot(X,clusters,centroids,y) to plot the data and the
clustering.
- Type help(clusterplot) to learn more about how to use the clustering
plot tool in the toolbox.
- To be more robust against different initial conditions, you can run multiple
iterations of clustering with different initial centroid seeds, see the 'n_init'
parameter of k_means().
- If you are stuck, take a look at the solution in ex9_1_1.py.

-- Does the clustering coincide with the true classes?
It typically does, especially in the case n_init=10 (default)

-- Try running your code several times (with n_init set to 1) to show that the algorithm can fail if the initial conditions are poor.
The initial conditions set below result in a failing algorithm

-- Try also the data sets synth2, synth3, and synth4.
They do not exist (?)
"""
from pylab import *
from scipy.io import loadmat
from toolbox_02450 import clusterplot
from sklearn.cluster import k_means

### my code ###

mat = loadmat('../Data/synth1.mat')
X = mat['X']
y = mat['y']
K = 4

# with the following values set the resulting clustering does not concide with the true classes
centroids, clusters, inertia = k_means(X,K
    n_init=1,
    init=np.matrix([[1,1],[1,1],[1,1],[1,1]]))

figure()
clusterplot(X, clusters, centroids, y)
show()

### example code ###

"""
# Load Matlab data file and extract variables of interest
mat_data = loadmat('../Data/synth1.mat')
X = np.matrix(mat_data['X'])
y = np.matrix(mat_data['y'])
attributeNames = [name[0] for name in mat_data['attributeNames'].squeeze()]
classNames = [name[0][0] for name in mat_data['classNames']]
N, M = X.shape
C = len(classNames)

# Number of clusters:
K = 4

# K-means clustering:
centroids, cls, inertia = k_means(X,K)
    
# Plot results:
figure(figsize=(14,9))
clusterplot(X, cls, centroids, y)
show()

"""