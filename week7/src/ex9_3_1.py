# exercise 9.1.1
"""
Load the Old Faithful data from Data/faithful. Analyze the data by k-means
visually inspect the labeling of the data points.
-- What happens if you increase K beyond the obvious K = 2?
Typically classes are split over a number of clusters.

Try to run the program a couple of times (resulting in different initial conditions).
-- Do you see the same solution every time?
No, for K=3 we see that the class that is split over 2 clusters differs.

The scaling of the variables can seriously affect the
results we get in clustering.

-- Discuss whether it is more reasonable to normalize the Old faithful data set?
It fully depends on the way the initial centroids are chosen.
We expect them to be picked randomly from within a bounding box around all points.
Hence, normalization should not affect the outcome of the k-means clustering.

-- Try normalizing the data and see whether the results of running k-means change.
For K=2, the results do not change. However, for greater values for K they do.
Apparently the initial centroids are picked in a different way than we expected.
Possibly the random initial coordinates are integers (speculation).
When operating in the range between 0 and 1 it seems better for them to be of type float.
"""
from pylab import *
from scipy.io import loadmat
from toolbox_02450 import clusterplot
from sklearn.cluster import k_means

### my code ###

mat = loadmat('../Data/faithful.mat')
X = mat['X']
y = mat['y']
K = 2

Xnorm = map(lambda x: [
    (x[0] - X.min(0)[0]) / (X.max(0)[0] - X.min(0)[0]),
    (x[1] - X.min(0)[1]) / (X.max(0)[1] - X.min(0)[1])], X)
centroids, clusters, inertia = k_means(Xnorm,K)

figure()
clusterplot(Xnorm, clusters, centroids, y)
show()