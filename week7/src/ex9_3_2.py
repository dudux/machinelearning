# exercise 9.2.1
'''
Key stuff : Dendogram and hierarchical clustering

Run hierarchical clustering of the Old Faithful data using the single and ward
linkage, with and without normalizing the data. 

>> Do you find support for a two
cluster model from the structure of the dendrograms?

When we use single method a couple are empty (148,264)  without split up. When we use ward method we got different outcomes. 
As dendrograms are similar to tree hierarchy, with only two values it's not very useful unless that the three is really large.

'''
from pylab import *
from scipy.io import loadmat
from toolbox_02450 import clusterplot
from scipy.cluster.hierarchy import linkage, fcluster, dendrogram

# Load Matlab data file and extract variables of interest
mat_data       = loadmat('../Data/faithful.mat')
X              = np.matrix(mat_data['X'])
y              = np.matrix(mat_data['y'])
attributeNames = [name[0] for name in mat_data['attributeNames'].squeeze()]
N, M           = X.shape


# Perform hierarchical/agglomerative clustering on data matrix
'''
Different methods what we can apply with:
    * ward     : uses the Ward variance minimization algorithm (This is also known as the incremental algorithm)
    * single   : This is also known as the Nearest Point Algorithm.
    * complete : This is also known by the Farthest Point Algorithm or Voor Hees Algorithm
    * average  
    * weighted 
    * centroid 
    * median  
'''
Method    = ['single','ward']
distances = ['minkowski','euclidean','cityblock','cosine','seuclidean']
Metric    = 'euclidean'
Z         = linkage(X, method=Method[1], metric=distances[1])

# Compute and display clusters by thresholding the dendrogram
Maxclust = 4

cls      = fcluster(Z, criterion='maxclust', t=Maxclust)
figure(1)
clusterplot(X, cls.reshape(cls.shape[0],1), y=y)

# Display dendrogram
max_display_levels=6
figure(2)
# Plots the hierarchical clustering as a dendrogram.
suptitle('Cluster Dendrogram for the Old Faithful dataset', fontweight='bold', fontsize=14);
dendrogram(Z, truncate_mode='level', p=max_display_levels)

show()

