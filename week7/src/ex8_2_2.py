# -*- coding: utf-8 -*-
"""
Key stuff : Bagging applied to decision trees == "Random forests"

The data set Data/synth5 we have used so far can trivially be fitted using a decision tree. 
Can you explain why? We will consider a different data set, which you can load using the command load Data/synth7.
Explore the data set and explain in what sense this is more challenging for a decision tree

Fit a random forest (an ensemble of bagged decision trees) to the data using L = 100 rounds of bagging. 
Compute the error rate (on the training set) and make a plot of the decision boundary.

"""

# exercise 8.2.2

from pylab import *
from scipy.io import loadmat
from sklearn.ensemble import RandomForestClassifier
from toolbox_02450 import dbplot, bootstrap
from bin_classifier_ensemble import BinClassifierEnsemble

# Load Matlab data file and extract variables of interest
mat_data       = loadmat('../Data/synth7.mat')
X              = np.matrix(mat_data['X'])
y              = np.matrix(mat_data['y'], dtype=int)
attributeNames = [name[0] for name in mat_data['attributeNames'][0]]
classNames     = [name[0][0] for name in mat_data['classNames']]
N, M           = X.shape
C              = len(classNames)


def scatterplot(X):
    # Plotting
    title('Scatterplot for Synthetical dataset')
    xlabel('X1.- First component values')
    ylabel('X2.- Second component values')
    
    # Scatter plot
    plot(X,'o')
    show()

#scatterplot(X)


# bootstrap samples
L = 100 

'''Fit a random forest (an ensemble of bagged decision trees) to the data using L = 100 rounds of bagging.
   Compute the error rate (on the training set) and make a plot of the decision boundary.'''
#----------------------------------------------------------------------------------------------------------
# Weights for selecting samples in each bootstrap
weights = np.ones((N,1),dtype=float)/N
# Storage of trained log.reg. classifiers fitted in each bootstrap
logits = [0]*L
votes  = np.zeros((N,1))

# For each round of bagging
for l in range(L):

    # Extract training set by random sampling with replacement from X and y
    X_train, y_train = bootstrap(X, y, N, weights)
    
    # Fit logistic regression model to training data and save result
    forest_clf = RandomForestClassifier(n_estimators =L)
    forest_clf.fit(X_train, y_train.A.ravel())
    logits[l]   = forest_clf
    y_est       = np.mat(forest_clf.predict(X)).T
    votes       = votes + y_est

    ErrorRate = (y!=y_est).sum(dtype=float)/N
    
# Compute the error rate (on the training set) 
ErrorRate = (y!=y_est).sum(dtype=float)/N
print('Error rate: {:.1f}%'.format(ErrorRate*100))

# and make a plot of the decision boundary.
ce = BinClassifierEnsemble(logits)
figure(1); dbplot(ce, X, y, 'auto', resolution=200)
show()