# exercise 9.2.1
'''
Key stuff : Dendogram and hierarchical clustering


Try changing the linkage method and see how it changes the dendrogram. Try
running your code several times to see if it generates exactly the same den-
drogram each time. Try also the data sets synth2, synth3,and synth4, and choose suitable distance 
measures for these data sets


synth2.mat--> Clusters are not well defined due to dispersed data.Maybe minkowski looks more suitable
synth4.mat--> Clusters are quite close
'''
from pylab import *
from scipy.io import loadmat
from toolbox_02450 import clusterplot
from scipy.cluster.hierarchy import linkage, fcluster, dendrogram

# Load Matlab data file and extract variables of interest
mat_data       = loadmat('../Data/synth1.mat')
#mat_data       = loadmat('../../week6/Data/synth2.mat')
X              = np.matrix(mat_data['X'])
y              = np.matrix(mat_data['y'])
attributeNames = [name[0] for name in mat_data['attributeNames'].squeeze()]
classNames     = [name[0][0] for name in mat_data['classNames']]
N, M           = X.shape
C              = len(classNames)


# Try distances
distances = ['minkowski','euclidean','cityblock','cosine','seuclidean']

# Perform hierarchical/agglomerative clustering on data matrix
Method = 'single'
Metric = 'euclidean'
Z = linkage(X, method=Method, metric=distances[1])

# Compute and display clusters by thresholding the dendrogram
Maxclust = 4
'''
Forms flat clusters from the hierarchical clustering defined by
 *criterion = how to form flat clusters:
     -incosistent (default)
     -distance : Forms flat clusters so that the original observations in each flat cluster have no greater a cophenetic distance than t.
     -maxclust : Finds a minimum threshold 
     -monocrit : Forms a flat cluster from a cluster node c with index i when monocrit[j] <= t.     
     -maxclust_monocrit : Forms a flat cluster from a cluster a
     
 * t = The threshold to apply when forming flat clusters
'''
cls      = fcluster(Z, criterion='maxclust', t=Maxclust)
figure(1)
clusterplot(X, cls.reshape(cls.shape[0],1), y=y)

# Display dendrogram

max_display_levels=6
figure(2)
# Plots the hierarchical clustering as a dendrogram.
suptitle('Cluster Dendrogram', fontweight='bold', fontsize=14);
dendrogram(Z, truncate_mode='level', p=max_display_levels)

show()
