# -*- coding: utf-8 -*-
"""
How well do you think we can identify the probabilities
P (x1 , x2 , x3 , x4 |Male) and
P (x1 , x2 , x3 , x4 |Female)
from the data at hand? 


Being :
    N_male_train                     the total number in the training data 
    N_male_train(x1 , x2 , x3 , x4 ) the number of female and male names with first letter x1 , second letter x2 , second last letter Female...


    
P (x1 , x2 , x3 , x4 |Male) = N_male_train (x1 , x2 , x3 , x4 )
                              ---------------------------------
                                      N_male_train

Knowing that we only consider names that contain at least four letters,we may observe a total of 2785 male and 4866 female names.
The dataset is not balanced properly due to the amount of female names in comparison with the male names. Around 2000 names of difference


Consider how likely it is that a given data set will contain enough 
training samples to allow us to accurately estimate these probabilities.

"""

