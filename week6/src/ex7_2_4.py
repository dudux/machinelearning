# -*- coding: utf-8 -*-
"""
Classify the names using a naive Bayes classifier. Use a uniform prior, assuming
male and female names are equally likely (i.e., P (Female) = P (Male) = 0.5).
Compute the classification error using 10-fold cross-validation.

Hints:
** Type help(sklearn.naive_bayes.MultinomialNB) to learn how to use a naive Bayes classifier in Python.
** As usual, use the sklearn.cross_validation module to set up the crossvalidation partitions.

>> Try classifying names based on only one of the letters in the name. You can do this by writing, e.g.,
X=X[:,0]; to choose the first letter. Show that the last letter is most useful for classifying names. Can you explain why?
    Names which end in -o will have high probability of being male, and otherwise names ending in -a could be female. There's no female names 
ending with -o.
TODO show!
"""

import numpy as np
import ex7_2_3 as data
from sklearn import cross_validation 
from sklearn.naive_bayes import MultinomialNB

n = data.n
X = data.X
y = np.array(data.y).ravel()

# create the Naive Bayes classiffier
clf = MultinomialNB(alpha=1.0, fit_prior=True, class_prior=None)
clf.fit(X, y) 
gender = clf.predict()

kf = cross_validation.KFold(n,n_folds=10)


#for train, test in kf:
#    print("%s %s" % (train, test))