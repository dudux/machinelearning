# -*- coding: utf-8 -*-
"""
KNN can also be used for regression by predicting the output of an observation
as the average of the output values of its nearest neighbors. 

Predict the alcohol content of wine using KNN regression. 

What is the optimal value for the number of nearest neighbors?
TODO


Hints:
* Load the wine2 data using the loadmat() function as before.
* To predict the alcohol content from the remaining inputs, split the data set
through y=X[:,10]; X = X[:, :10];. Remember to update N.
* Use the KNeighborsClassifier class to find the nearest neighbors.
* Predict each observation as the mean of the output value of its nearest
neighbors.

"""

import scipy.io
from sklearn.neighbors import KNeighborsClassifier

# neighbors
n = 5

mat     = scipy.io.loadmat('../../week5/Data/wine2.mat')
X       = mat['X']
y       = mat['y']
attribs = mat['attributeNames']

# Split up the dataset
y  = X[:,10]  # alcohol content  --> values between [8,14.9]
X  = X[:,:10]  # Rest 

# Udpate N
N,M = X.shape

# Each observation as mean 

knclassifier = KNeighborsClassifier(n_neighbors=n).fit(X, y)
neighbors    = knclassifier.kneighbors(X)
ndist, nid   = neighbors[0], neighbors[1]
#print ("k={0:2} with mean:{1:4.05}".format(k,knclassifier.predict(X).mean()))


'''
FEEDBACK

1. "for k in xrange(L-4,L+50):" Why is L used here?
2. For each datapoint, you predict it's alcohol content by by taking the mean of the the alcohol content of its neighbours.

2.1 What does knclassifier.predict(X) return? 
2.2 You should NOT use the training dataset to test. I.e. fitting on X and then predicting on X is wrong.

'''