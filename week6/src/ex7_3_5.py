# exercise 7.3.5

from pylab import *
from scipy.io import loadmat
import neurolab as nl
from sklearn import cross_validation
import scipy.linalg as linalg
from scipy import stats

# Load Matlab data file and extract variables of interest
mat_data = loadmat('../../week5/Data/wine2.mat')
attributeNames = [name[0] for name in mat_data['attributeNames'][0]]
X = np.matrix(mat_data['X'])
y = np.matrix(mat_data['y'])
N, M = X.shape
C = 2

# Normalize and compute Principal Components
Y = stats.zscore(X,0);
U,S,V = linalg.svd(Y,full_matrices=False)
V = mat(V).T

# Components to be included as features
k_pca = 8
X = X*V[:,0:k_pca]
N, M = X.shape

# Parameters for neural network classifier
n_hidden_units = 10     # number of hidden units
n_train = 1             # number of networks trained in each k-fold

learning_goal = 200.0   # stop criterion 1 (train mse to be reached)
max_epochs = 32         # stop criterion 2 (max epochs in training)

# K-fold crossvalidation
K = 5                   # only five folds to speed up this example
CV = cross_validation.KFold(N,K,shuffle=True)

# Variable for classification error
errors = np.zeros(K)
error_hist = np.zeros((max_epochs,K))
bestnet = list()
k=0
for train_index, test_index in CV:
    print('\nCrossvalidation fold: {0}/{1}'.format(k+1,K))    
    
    # extract training and test set for current CV fold
    X_train = X[train_index,:]
    y_train = y[train_index,:]
    X_test = X[test_index,:]
    y_test = y[test_index,:]
    
    best_train_error = 1e100
    for i in range(n_train):
        # Create randomly initialized network with 2 layers
        ann = nl.net.newff([[-1, 1]]*M, [n_hidden_units, 1], [nl.trans.TanSig(),nl.trans.TanSig()])
        # train network
        train_error = ann.train(X_train, y_train, goal=learning_goal, epochs=max_epochs, show=round(max_epochs/8))
        if train_error[-1]<best_train_error:
            bestnet.append(ann)
            best_train_error = train_error[-1]
            error_hist[range(len(train_error)),k] = train_error
    
    y_est = bestnet[k].sim(X_test)
    y_est = (y_est>.5).astype(int)
    errors[k] = (y_est!=y_test).sum().astype(float)/y_test.shape[0]
    k+=1
    

# Print the average classification error rate
print('Error rate: {0}%'.format(100*mean(errors)))

# Display the decision boundry for the last crossvalidation fold.
# (create grid of points, compute network output for each point, color-code and plot).

grid_range = [-1, 20, -1, 5]; delta = 0.1; levels = 100
a = arange(grid_range[0],grid_range[1],delta)
b = arange(grid_range[2],grid_range[3],delta)
A, B = meshgrid(a, b)
values = np.zeros(A.shape)

show()