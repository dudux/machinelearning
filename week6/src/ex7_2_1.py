# -*- coding: utf-8 -*-
"""

author    : Eduardo Novella && Carlo Meijer 
Key stuff : Naive bayes


y1 = female
y2 = male

name = 'Eduardo'

x1 = 'E' => First letter
x2 = 'd' => Second letter
x3 = 'd' => Second last letter
X4 = 'o' => last letter                



In order to classify names as male or female according to the above procedure,
we need to count the number of times given letter combinations occurred 
in the male and female names in the training data, 

>>How many times each of the letter combinations
(x1 , x2 , x3 , x4 ) = (a, a, a, a)
(x1 , x2 , x3 , x4 ) = (a, a, a, b)
(x1 , x2 , x3 , x4 ) = (z, z, z, z) occurred. 
If we sort the list of lists, and later the list is agrouped by using 'groupby', 
we can remove many duplicates values. Therefore, 3575 duplicates were removed

>> How many different letter combinations do we have to evaluate?
In the worst case we would have 26^4 = 456976 but how we only have 7651 names, this is the worst case.
However, if we remove duplicates we obtain 4076 occurrences.

"""
from itertools import  groupby
import numpy as np

# Load list of names from files
fmale   = open('../Data/male.txt','r')
ffemale = open('../Data/female.txt','r')
mnames  = fmale.readlines(); fnames = ffemale.readlines()
names   = mnames + fnames
gender  = [0]*len(mnames) + [1]*len(fnames)
fmale.close(); ffemale.close()

# Extract X, y and the rest of variables. Include only names of >4 characters.
X = np.mat(np.zeros((len(names),4)))
y = np.mat(np.zeros((len(names),1)))
n=0
for i in range(0,len(names)):
    name = names[i].strip().lower()
    if len(name)>3:
        X[n,:] = [ord(name[0])-ord('a')+1, ord(name[1])-ord('a')+1, ord(name[-2])-ord('a')+1, ord(name[-1])-ord('a')+1]
        y[n,0] = gender[i]
        n+=1


# Convert numpy.matrix to list
x = X.tolist()
# remove duplicate lists' list 
x = list([k for k,v in groupby(sorted(x))])
print "Total Names   : {0} \nNames_>4digit : {1} \nuniques       : {2} \nRepeated      : {3}".format(len(X),n,len(x),n-len(x))

X = X[0:n,:]
y = y[0:n,:]

N, M           = X.shape  # row & columns
C              = 2        # number of classes
attributeNames = ['1st letter', '2nd letter', 'Next-to-last letter', 'Last letter']
classNames     = ['Female', 'Male']

