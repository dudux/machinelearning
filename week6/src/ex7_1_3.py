# exercise 7.1.3
'''

Leave-one-out cross-validation can be eficiently implemented by finding 
all the nearest neighbors for the largest considered value of k and then evaluating the
error-rate for each choice of k without having to re-estimate the nearest neigh-
bors. Inspect the script ex7_1_3.py and explain how all nearest neighbors are
found using the function kneighbors(). The observations are subsequently
classified by a majority voting and when ties the observations are assigned to
the first class having the largest number of observations.

Run the script and compare the computation time and results to the results obtained when running
your script from before.

For k between [18,20] neighbors we apprecciatea a really low error rate.

'''


from pylab import *
from sklearn.neighbors import KNeighborsClassifier
from sklearn import datasets

# requires data from exercise 4.1.1 
#import ex4_1_1 as data
iris = datasets.load_iris()
X    = iris.data
y    = iris.target
N, M = X.shape
C    = len(iris.target_names) 

# Maximum number of neighbors
L=40

# Cross-validation not necessary. Instead, compute matrix of nearest neighbor
# distances between each pair of data points ...
knclassifier = KNeighborsClassifier(n_neighbors=L+1, warn_on_equidistant=False).fit(X, y)
neighbors    = knclassifier.kneighbors(X)

# .. and extract matrix where each row contains class labels of subsequent neighbours
# (sorted by distance)
ndist, nid = neighbors[0], neighbors[1]
nclass     = y[nid].flatten().reshape(N,L+1)

# Use the above matrix to compute the class labels of majority of neighbors
# (for each number of neighbors l), and estimate the test errors.
errors       = np.zeros(L)
nclass_count = np.zeros((N,C))
for l in range(1,L+1):
    for c in range(C):
        nclass_count[:,c] = sum(nclass[:,1:l+1]==c,1).ravel()  # Old line: ......1).A.ravel()
    y_est = np.argmax(nclass_count,1);
    errors[l-1] = (y_est!=y.ravel()).sum()                     # Old line ........A.ravel()).sum....

    
# Plot the classification error rate
figure(1)
plot(100*errors/N)
xlabel('Number of neighbors')
ylabel('Classification error rate (%)')

figure(2)
imshow(nclass, cmap='binary', interpolation='None'); xlabel("k'th neighbor"); ylabel('data point'); title("Neighbors class matrix");

show()
