# -*- coding: utf-8 -*-
"""
@author   : Eduardo Novella && Carlo Meijer
Key stuff : leave-one-out cross-validation

Load the Iris data into Python. Use leave-one-out cross-validation to estimate the number of neighbors "k" , 
for the k-nearest neighbor classifier. 
Plot the cross-validated average classification error as a function of k for k = 1, . . . , 40. 

Hints
1.- To load the Iris data, you can run your solution to Exercise 4.1.1.
2.- Use LeaveOneOut cross-validation from the module sklearn.cross_validation.
3.- As before, use the KNeighborsClassifier class for k-nearest neighbor classification.


LeaveOneOut (or LOO) is a simple cross-validation. Each learning set is created by taking all the samples except one, 
the test set being the sample left out. Thus, for n samples, we have n different learning sets and n different tests set. 

"""

from sklearn.cross_validation import LeaveOneOut
from sklearn.neighbors import KNeighborsClassifier
import ex4_1_1 as data

# neighbors
n = 5

X = data.X

loo = LeaveOneOut(n=len(X))

# Plot the cross-validated average classification error as a function of k for k = 1,..., 40
for k in xrange(1,41):
    knn = KNeighborsClassifier(n_neighbors=k) 
    
