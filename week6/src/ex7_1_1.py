# -*- coding: utf-8 -*-
"""
@author   : Eduardo Novella && Carlo Meijer
Key stuff : k -nearest neighbors (KNN) method for classification and Confusion matrix

    
For each of the four synthetic data sets (synth1.mat,....,synth4.mat), do the following. 
Load the dataset into Python and examine it by making a scatter plot.
Classify the test data X_test using a k-nearest classifier.

Choose a suitable distance measure (you should consider the distance measures "euclidean"
and "cityblock").
Choose a suitable number of neighbors. Examine the accuracy and error rate.


>> Which distance measures worked best for the four problems? Can you explain why?
TODO

>> How many neighbors were needed for the four problems?
TODO

>> Can you give an example of when it would be good to use a large/small number of neighbors?
Consider e.g. when clusters are well separated versus when they are overlapping.
TODO

"""

import scipy.io
import numpy as np
from pylab import *
from sklearn.metrics import confusion_matrix
from sklearn.neighbors import KNeighborsClassifier

# neighbours
n       = 5
metrics = [ 'euclidean', 'cityblock']

def loadX(i):
    '''
    Load data
    '''
    name    = '../Data/synth{0}.mat'.format(i)
    mat     = scipy.io.loadmat(name)
    X       = mat['X']
    y       = np.ravel(mat['y'])
    X_test  = mat['X_test']
    X_train = mat['X_train']
    y_test  = np.ravel(mat['y_test'])
    y_train = np.ravel(mat['y_train'])
    return X,y,X_test,X_train,y_test,y_train

def calculateAccuracy(y_test,y_pred):
    n = len(y_test)
    e = np.zeros(n)  # error array
        
    for i in xrange(n):
        if ( y_test[i]!=y_pred[i]): # error 1
            e[i] = 1
        else:                       # error 0 because the prediction is fine
            e[i] = 0
    return e.mean()
    
fig = figure(1)
fig.suptitle('Scatter plot for the four synthetic datasets')

for i in xrange(4):
    X,y,X_test,X_train,y_test,y_train = loadX(i+1)
    for m in metrics:
        knncsf = KNeighborsClassifier(n_neighbors=n, weights='uniform', metric=m) 
        knncsf.fit(X_train,y_train)
        y_pred = knncsf.predict(X_test)
        
        errate = calculateAccuracy(y_test,y_pred)
        print (">Error rate for synth{0}.mat by using {1} and neighbours K={2} : {3}".format(i+1,m,n,errate))
    print    
    subplot(2,2,i+1)    
    plot(X,'.')
    xlabel('Attribute A');
    ylabel('Attribute B');
  

show()   

figure(2)
C = confusion_matrix(y_test,y_pred)
imshow(C)  

'''
FEEDBACK

1. For each dataset make a scatterplot of the X matrix. There are only two features so this should be rather easy.
2. NEVER(!) fit a model on the test dataset! knn.fit(X_test,y_test) is wrong!
3. Don't use knn.score for the score. Write your own score function. You can look up the definitions of accuracy and error rate on wikipedia.
'''