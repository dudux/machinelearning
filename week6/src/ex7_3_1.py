# -*- coding: utf-8 -*-
"""


In Figure 1, a network with two input units, two hidden units and one output
unit has been trained based on linear functions as transfer function. That is, the
h1 = g (hidden) (1.5 + 10x1 − 8x2 ) and the output of the second hidden unit is
 h2 = g(hidden) (−8 + 1x1 + 3x2 ) with g (hidden) (t) = t .

The final output of the neural network is given by y^(est)= g (output) (−5+7h1 +1h2 )
with g(output)(t) = t . What is the output of the network if x1 = 1 and x2 = 0?
And what if x1 = 0.5 and x2 = 0.5?


As both inputs have been trained based on linear functions as transfer function. For that,
both are linear and we do not need to calculate tan(t)

"""

import math

x1=[1,0.5]
x2=[0,0.5]

for i in xrange(len(x1)):
    h1    = 1.5 + 10*x1[i]-8*x2[i] # math.tan(1.5 + 10*x1[i]-8*x2[i])
    h2    = -8+1*x1[i]+3*x2[i]
    y_est = -5+7*h1 +1*h2
    print "(x1,x2)=({0:3},{1:3}) ===> Output y_est: {2}".format(x1[i],x2[i],y_est)

